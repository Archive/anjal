/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef EM_MOZEMBED_STREAM_H
#define EM_MOZEMBED_STREAM_H

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define EM_MOZEMBED_STREAM_TYPE     (em_mozembed_stream_get_type ())
#define EM_MOZEMBED_STREAM(obj)     (CAMEL_CHECK_CAST((obj), EM_MOZEMBED_STREAM_TYPE, EMMozEmbedStream))
#define EM_MOZEMBED_STREAM_CLASS(k) (CAMEL_CHECK_CLASS_CAST ((k), EM_MOZEMBED_STREAM_TYPE, EMMozEmbedStreamClass))
#define EM_IS_MOZEMBED_STREAM(o)    (CAMEL_CHECK_TYPE((o), EM_MOZEMBED_STREAM_TYPE))

#include "mail/em-sync-stream.h"
#include <gtkmozembed.h>

typedef struct _EMMozEmbedStream {
	EMSyncStream sync;

	GtkMozEmbed *view;
	GtkWidget *body;
	guint destroy_id;
	int flags;
	gboolean open;
} EMMozEmbedStream;

typedef struct {
	EMSyncStreamClass parent_class;

} EMMozEmbedStreamClass;


CamelType    em_mozembed_stream_get_type (void);

CamelStream *em_mozembed_stream_new(GtkMozEmbed *, GtkWidget *);
void em_mozembed_stream_set_flags (EMMozEmbedStream *emhs, int flags);
GtkWidget * em_mozembed_stream_get_body (EMMozEmbedStream *emms);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* EM_MOZEMBED_STREAM_H */
