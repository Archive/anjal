/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@gnome.org>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#include "mail-float-bar.h"

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#define MAIL_FLOAT_BAR_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), MAIL_FLOAT_BAR_TYPE, MailFloatBarPrivate))

struct _MailFloatBarPrivate
{
	GtkWidget *toolbar;
};

static GObjectClass *parent_class = NULL;

static void mail_float_bar_class_init(MailFloatBarClass *klass);
static void mail_float_bar_init(MailFloatBar *facet);

GType
mail_float_bar_get_type(void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		static const GTypeInfo mail_float_bar_info =
		{
			sizeof (MailFloatBarClass),
			NULL,
			NULL,
			(GClassInitFunc) mail_float_bar_class_init,
			NULL,
			NULL,
			sizeof (MailFloatBar),
			0,
			(GInstanceInitFunc) mail_float_bar_init
		};

		type = g_type_register_static (G_TYPE_OBJECT,
					       "MailFloatBar",
					       &mail_float_bar_info, 0);
	}

	return type;
}



static void
mail_float_bar_class_init(MailFloatBarClass *klass)
{

	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	/* GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);*/

	parent_class = g_type_class_peek_parent (klass);

	g_type_class_add_private (object_class, sizeof(MailFloatBarPrivate));
}

static void
mail_float_bar_init(MailFloatBar *editor)
{
	MailFloatBarPrivate *priv;

	priv = MAIL_FLOAT_BAR_GET_PRIVATE (editor);
}

static void
mfb_construct (MailFloatBar *bar, GtkWidget *top)
{
	GtkWidget *toolbar;
	GtkToolItem *item;
	GtkWidget *window = gtk_window_new (GTK_WINDOW_POPUP), *box;
	int x, y;

	MailFloatBarPrivate *priv;

	toolbar = gtk_hbox_new (FALSE, 2);
	priv = MAIL_FLOAT_BAR_GET_PRIVATE (bar);
	priv->toolbar = toolbar;

	/*Create items.*/
	item = gtk_tool_button_new (gtk_label_new(_("Hide Folder Tree")), NULL);
	gtk_box_pack_start ((GtkBox *)toolbar, (GtkWidget *)item, FALSE, FALSE, 0);

	item = gtk_tool_button_new (gtk_image_new_from_icon_name("go-previous", GTK_ICON_SIZE_MENU), NULL);
	gtk_box_pack_start ((GtkBox *)toolbar, (GtkWidget *)item, FALSE, FALSE, 0);

	item = gtk_tool_button_new (gtk_image_new_from_icon_name("go-next", GTK_ICON_SIZE_MENU), NULL);
	gtk_box_pack_start ((GtkBox *)toolbar, (GtkWidget *)item, FALSE, FALSE, 0);

	item = gtk_tool_button_new (gtk_image_new_from_icon_name("gtk-close", GTK_ICON_SIZE_MENU), NULL);
	gtk_box_pack_start ((GtkBox *)toolbar, (GtkWidget *)item, FALSE, FALSE, 0);

	box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start ((GtkBox *)box, toolbar, TRUE, TRUE, 4);
	gtk_widget_set_size_request (box, 200, 32);
	gtk_window_set_default_size ((GtkWindow *)window, 250, 32);
	gtk_container_add((GtkContainer *)window, box);
	gtk_window_set_transient_for ((GtkWindow *)window, (GtkWindow *)top);
	gtk_widget_show_all(window);
	gtk_window_get_position ((GtkWindow *)top, &x, &y);

	gtk_window_move ((GtkWindow *)window, x - 125 + (top->allocation.width/2), y + top->allocation.height-32);
}
MailFloatBar * mail_float_bar_new(GtkWidget *top)
{
	MailFloatBar *bar = (MailFloatBar *) g_object_new(mail_float_bar_get_type(), NULL);

	mfb_construct (bar, top);

	return bar;
}

