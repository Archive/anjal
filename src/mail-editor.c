/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Johnny Jacob <jjohnny@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#include "mail-editor.h"

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <webkit/webkit.h>
#include <gdk/gdkkeysyms.h>

#define MAIL_EDITOR_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), MAIL_EDITOR_TYPE, MailEditorPrivate))

struct _MailEditorPrivate
{
	WebKitWebView *editor; /*TODO : Subclass from webkits ?*/
	GtkToolbar *toolbar;
};

static GtkVBoxClass *parent_class = NULL;

static void mail_editor_class_init(MailEditorClass *klass);
static void mail_editor_init(MailEditor *facet);

GType
mail_editor_get_type(void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0))
	{
		static const GTypeInfo mail_editor_info =
		{
			sizeof (MailEditorClass),
			NULL,
			NULL,
			(GClassInitFunc) mail_editor_class_init,
			NULL,
			NULL,
			sizeof (MailEditor),
			0,
			(GInstanceInitFunc) mail_editor_init
		};

		type = g_type_register_static (GTK_TYPE_VBOX,
					       "MailEditor",
					       &mail_editor_info, 0);
	}

	return type;
}

GtkWidget* mail_editor_new()
{
	return GTK_WIDGET(g_object_new(mail_editor_get_type(), NULL));
}


static void
mail_editor_class_init(MailEditorClass *klass)
{

	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	/* GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);*/

	parent_class = g_type_class_peek_parent (klass);

	g_type_class_add_private (object_class, sizeof(MailEditorPrivate));
}

static void
tb_bold_cb (GtkToggleToolButton *button, MailEditor *editor)
{
	MailEditorPrivate *priv;
	WebKitWebFrame *frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_focused_frame (priv->editor);

	if (!frame)
		frame = webkit_web_view_get_main_frame (priv->editor);

	g_return_if_fail (frame != NULL);

	webkit_web_frame_execute_command (frame, "ToggleBold");
}

static void
tb_underline_cb (GtkToggleToolButton *button, MailEditor *editor)
{
	MailEditorPrivate *priv;
	WebKitWebFrame *frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_focused_frame (priv->editor);

	if (!frame)
		frame = webkit_web_view_get_main_frame (priv->editor);

	g_return_if_fail (frame != NULL);

	webkit_web_frame_execute_command (frame, "ToggleUnderline");
}

static void
tb_italics_cb (GtkToggleToolButton *button, MailEditor *editor)
{
	MailEditorPrivate *priv;
	WebKitWebFrame *frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_focused_frame (priv->editor);

	if (!frame)
		frame = webkit_web_view_get_main_frame (priv->editor);

	g_return_if_fail (frame != NULL);

	webkit_web_frame_execute_command (frame, "ToggleItalic");
}

static void
tb_justify_left_cb (GtkToggleToolButton *button, MailEditor *editor)
{
	MailEditorPrivate *priv;
	WebKitWebFrame *frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_focused_frame (priv->editor);

	if (!frame)
		frame = webkit_web_view_get_main_frame (priv->editor);

	g_return_if_fail (frame != NULL);

	webkit_web_frame_execute_command (frame, "JustifyLeft");
}

static void
tb_justify_center_cb (GtkToggleToolButton *button, MailEditor *editor)
{
	MailEditorPrivate *priv;
	WebKitWebFrame *frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_focused_frame (priv->editor);

	if (!frame)
		frame = webkit_web_view_get_main_frame (priv->editor);

	g_return_if_fail (frame != NULL);

	webkit_web_frame_execute_command (frame, "JustifyCenter");
}

static void
tb_justify_right_cb (GtkToggleToolButton *button, MailEditor *editor)
{
	MailEditorPrivate *priv;
	WebKitWebFrame *frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_focused_frame (priv->editor);

	if (!frame)
		frame = webkit_web_view_get_main_frame (priv->editor);

	g_return_if_fail (frame != NULL);

	webkit_web_frame_execute_command (frame, "JustifyRight");
}

static void
tb_indent_cb (GtkToggleToolButton *button, MailEditor *editor)
{
	MailEditorPrivate *priv;
	WebKitWebFrame *frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_focused_frame (priv->editor);

	if (!frame)
		frame = webkit_web_view_get_main_frame (priv->editor);

	g_return_if_fail (frame != NULL);

	webkit_web_frame_execute_command (frame, "Indent");
}

static void
tb_outdent_cb (GtkToggleToolButton *button, MailEditor *editor)
{
	MailEditorPrivate *priv;
	WebKitWebFrame *frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_focused_frame (priv->editor);

	if (!frame)
		frame = webkit_web_view_get_main_frame (priv->editor);

	g_return_if_fail (frame != NULL);

	webkit_web_frame_execute_command (frame, "Outdent");
}

static GtkToolbar*
mail_editor_construct_html_toolbar (MailEditor *editor)
{
	GtkToolbar *toolbar;
	GtkToolItem *item;

	MailEditorPrivate *priv;

	toolbar = (GtkToolbar *)gtk_toolbar_new ();
	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	/*Create items.*/
	item = gtk_tool_button_new_from_stock (GTK_STOCK_INDENT);
	gtk_toolbar_insert (toolbar, item, 0);
	g_signal_connect (item, "clicked", G_CALLBACK(tb_indent_cb), editor);

	item = gtk_tool_button_new_from_stock (GTK_STOCK_UNINDENT);
	gtk_toolbar_insert (toolbar, item, 0);
	g_signal_connect (item, "clicked", G_CALLBACK(tb_outdent_cb), editor);

	item = gtk_tool_button_new_from_stock (GTK_STOCK_JUSTIFY_RIGHT);
	gtk_toolbar_insert (toolbar, item, 0);
	g_signal_connect (item, "clicked", G_CALLBACK(tb_justify_right_cb), editor);

	item = gtk_tool_button_new_from_stock (GTK_STOCK_JUSTIFY_CENTER);
	gtk_toolbar_insert (toolbar, item, 0);
	g_signal_connect (item, "clicked", G_CALLBACK(tb_justify_center_cb), editor);

	item = gtk_tool_button_new_from_stock (GTK_STOCK_JUSTIFY_LEFT);
	gtk_toolbar_insert (toolbar, item, 0);
	g_signal_connect (item, "clicked", G_CALLBACK(tb_justify_left_cb), editor);

	item = gtk_tool_button_new_from_stock (GTK_STOCK_BOLD);
	gtk_toolbar_insert (toolbar, item, 0);
	g_signal_connect (item, "clicked", G_CALLBACK(tb_bold_cb), editor);

	item = gtk_tool_button_new_from_stock (GTK_STOCK_UNDERLINE);
	gtk_toolbar_insert (toolbar, item, 0);
	g_signal_connect (item, "clicked", G_CALLBACK(tb_underline_cb), editor);

	item = gtk_tool_button_new_from_stock (GTK_STOCK_ITALIC);
	g_signal_connect (item, "clicked", G_CALLBACK(tb_italics_cb), editor);
	gtk_toolbar_insert (toolbar, item, 0);

	return toolbar;
}

static void
editor_loaded_cb (WebKitWebView *view, WebKitWebFrame *frame, MailEditor *editor)
{
}

static void
mail_editor_init(MailEditor *editor)
{
	MailEditorPrivate *priv;
	gchar *index_html;
	GtkWidget *scroller, *vbox;

	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	priv->editor = (WebKitWebView*)webkit_web_view_new ();
	scroller = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroller), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller),
				GTK_POLICY_NEVER,
				GTK_POLICY_AUTOMATIC);

	/*Dummy html doc*/
	index_html = "<html><body></body></html>";

	webkit_web_view_load_html_string (priv->editor, index_html, NULL);
	webkit_web_view_set_editable (priv->editor, TRUE);

	g_signal_connect (priv->editor, "load-finished", G_CALLBACK(editor_loaded_cb), editor);

	gtk_widget_show ((GtkWidget *)priv->editor);

	priv->toolbar = mail_editor_construct_html_toolbar (editor);
	gtk_widget_show_all ((GtkWidget *)priv->toolbar);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start ((GtkBox *)vbox, (GtkWidget *)priv->toolbar, FALSE, TRUE, 0);
	gtk_box_pack_start ((GtkBox *)vbox, scroller, TRUE, TRUE, 0);
	gtk_container_add ((GtkContainer *)scroller, (GtkWidget *)priv->editor);
	gtk_widget_show (scroller);
	gtk_container_add (GTK_CONTAINER (editor), vbox);
	gtk_widget_show (vbox);
}

gchar *
mail_editor_get_text_plain (MailEditor *editor,
			      gsize *length)
{
	MailEditorPrivate *priv;
	WebKitWebFrame* frame;
	gchar *contents = NULL;

	g_return_val_if_fail (IS_MAIL_EDITOR (editor), NULL);

	priv = MAIL_EDITOR_GET_PRIVATE (editor);

	frame = webkit_web_view_get_main_frame (priv->editor);
	contents = webkit_web_frame_get_inner_text (frame);

	*length = g_utf8_strlen (contents, -1); /*FIXME*/

	return contents;
}

void
mail_editor_set_changed (MailEditor *editor, gboolean set)
{
}

gboolean
mail_editor_get_html_mode (editor)
{
	/*TODO : > */
	return TRUE;
}

void
mail_editor_run_command (MailEditor *editor, gchar *command)
{
	
}

WebKitWebView *
mail_editor_get_webview (MailEditor *editor)
{
	MailEditorPrivate *priv;

	g_return_val_if_fail (IS_MAIL_EDITOR (editor), NULL);
	
	priv = MAIL_EDITOR_GET_PRIVATE (editor);
	return priv->editor;
}

void
mail_editor_set_text_html (MailEditor *editor, gchar *text, gint length)
{
	MailEditorPrivate *priv;
	WebKitWebFrame* frame;

	g_return_if_fail (IS_MAIL_EDITOR (editor));

	priv = MAIL_EDITOR_GET_PRIVATE (editor);
	frame = webkit_web_view_get_main_frame (priv->editor);
	webkit_web_frame_load_string (frame, text, NULL, NULL, "");
}

gchar*
mail_editor_get_html (MailEditor *editor, gsize *length)
{
	MailEditorPrivate *priv;
	WebKitWebFrame* frame;
	gchar *contents = NULL;

	g_return_val_if_fail (IS_MAIL_EDITOR (editor), NULL);

	priv = MAIL_EDITOR_GET_PRIVATE (editor);
	*length = 0;

	frame = webkit_web_view_get_main_frame (priv->editor);
	contents = webkit_web_frame_get_inner_text (frame);
	contents = g_strdup_printf ("<html>%s</html>", contents);
	*length = g_utf8_strlen (contents, -1); /*FIXME ?*/

	return contents;
}
