/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 * Copyright (C) 2009 Intel Corporation (www.intel.com)
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <config.h>

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gi18n.h>
#include <libedataserverui/e-passwords.h>
#include <mail/mail-mt.h>
#include "mail-shell.h"
#include <gconf/gconf-client.h>
#include <libedataserver/e-categories.h>
#include <dbus/dbus-glib.h>

#ifdef G_OS_WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#if HAVE_UNIQUE
#include <unique/unique.h>
#endif

#if HAVE_ANERLEY|HAVE_CLUTTER
#include <clutter-gtk/clutter-gtk.h>
#if HAVE_ANERLEY
#include <nbtk/nbtk.h>
#endif
#endif

gboolean windowed = FALSE;
gboolean anjal_icon_decoration = FALSE;
gboolean default_app =  FALSE;
MailShell *main_window;
static gchar **remaining_args;
extern char *shell_moduledir;

#define GCONF_KEY_MAILTO_ENABLED "/desktop/gnome/url-handlers/mailto/enabled"
#define GCONF_KEY_MAILTO_COMMAND "/desktop/gnome/url-handlers/mailto/command"
#define ANJAL_MAILTO_COMMAND "anjal %s"

#if HAVE_UNIQUE
static UniqueResponse
mail_message_received_cb (UniqueApp         *app G_GNUC_UNUSED,
                         gint               command,
                         UniqueMessageData *message_data,
                         guint              time_ G_GNUC_UNUSED,
                         gpointer           user_data)
{
	 gchar *url;
	 GtkWindow *window = (GtkWindow *) user_data;
	 char *args[2];
	 
	 switch (command) {
	 case UNIQUE_ACTIVATE :
		  gtk_window_deiconify (window);
		  gtk_window_present (window);
		  return UNIQUE_RESPONSE_OK;

	 case UNIQUE_NEW :
		  return UNIQUE_RESPONSE_OK;

	 case UNIQUE_OPEN :
		  url = unique_message_data_get_text (message_data);
		  args[0] = url;
		  args[1] = NULL;
		  mail_shell_set_cmdline_args ((MailShell *)window, args);
		  mail_shell_handle_cmdline ((MailShell *)window);
		  g_free (url);
		  gdk_window_raise (((GtkWidget *)window)->window);
		  gtk_window_deiconify (window);
		  gtk_window_present (window);

		  return UNIQUE_RESPONSE_OK;
	 }

	 return UNIQUE_RESPONSE_PASSTHROUGH;
}
#endif

static void
check_and_set_default_mail (void)
{
	GConfClient *client = gconf_client_get_default ();
	gchar *mailer;

	mailer  = gconf_client_get_string(client, GCONF_KEY_MAILTO_COMMAND, NULL);
	if (mailer && *mailer && (strcmp (mailer, ANJAL_MAILTO_COMMAND) == 0)) {
		g_object_unref (client);
		return; /* Anjal is the default mailer */
	}
	
	gconf_client_set_bool(client, GCONF_KEY_MAILTO_ENABLED, TRUE, NULL);
	gconf_client_set_string(client, GCONF_KEY_MAILTO_COMMAND, ANJAL_MAILTO_COMMAND, NULL);
	g_object_unref (client);
}

static gboolean
idle_cb (MailShell *mshell G_GNUC_UNUSED)
{
	EShell *shell;

	shell = e_shell_get_default ();
	
	if (default_app) {
		check_and_set_default_mail ();
	}

#if HAVE_UNIQUE	
	if (unique_app_is_running (UNIQUE_APP (shell))) {
		   gboolean cmd_line =  (remaining_args && remaining_args[0] != NULL);
		   
		  if (!cmd_line)
			unique_app_send_message (UNIQUE_APP(shell), UNIQUE_ACTIVATE, NULL);
		  else {
			  UniqueMessageData *data = unique_message_data_new ();
			  unique_message_data_set_text (data, remaining_args[0], -1);
			  unique_app_send_message (UNIQUE_APP(shell), UNIQUE_OPEN, data);
			  unique_message_data_free (data);
		  }

		  gtk_main_quit ();
		  return FALSE;
	  }
#endif

	main_window = mail_shell_new (shell, TRUE);
	gtk_widget_show ((GtkWidget *)main_window);
	mail_shell_set_cmdline_args ((MailShell *)main_window, remaining_args);
#if HAVE_UNIQUE	
	g_signal_connect (UNIQUE_APP(shell), "message-received",
                    G_CALLBACK (mail_message_received_cb),  main_window);
#endif
	return FALSE;
}


static void
categories_icon_theme_hack (void)
{
	GtkIconTheme *icon_theme;
	const gchar *category_name;
	const gchar *filename;
	gchar *dirname;

	/* XXX Allow the category icons to be referenced as named
	 *     icons, since GtkAction does not support GdkPixbufs. */

	/* Get the icon file for some default category.  Doesn't matter
	 * which, so long as it has an icon.  We're just interested in
	 * the directory components. */
	category_name = _("Birthday");
	filename = e_categories_get_icon_file_for (category_name);
	g_return_if_fail (filename != NULL && *filename != '\0');

	/* Extract the directory components. */
	dirname = g_path_get_dirname (filename);

	/* Add it to the icon theme's search path.  This relies on
	 * GtkIconTheme's legacy feature of using image files found
	 * directly in the search path. */
	icon_theme = gtk_icon_theme_get_default ();
	gtk_icon_theme_append_search_path (icon_theme, dirname);

	g_free (dirname);
}

static void
shell_window_destroyed_cb (EShell *shell)
{
	if (e_shell_get_watched_windows (shell) == NULL)
		gtk_main_quit ();
}

static void
create_default_shell (void)
{
	EShell *shell;
	GConfClient *client;
	gboolean online = TRUE;
	const gchar *key;
	GError *error = NULL;

	client = gconf_client_get_default ();
	key = "/apps/evolution/shell/start_offline";

	/* Requesting online or offline mode from the command-line
	 * should be persistent, just like selecting it in the UI. */

	/*if (start_online) {
		online = TRUE;
		gconf_client_set_bool (client, key, FALSE, &error);
	} else if (start_offline) {
		online = FALSE;
		gconf_client_set_bool (client, key, TRUE, &error);
	} else*/ {
		gboolean value;

		value = gconf_client_get_bool (client, key, &error);
		if (error == NULL)
			online = !value;
	}

	if (error != NULL) {
		g_warning ("%s", error->message);
		g_error_free (error);
	}

	shell = g_object_new (
		E_TYPE_SHELL,
		"name", "org.gnome.Anjal",
		"module-directory", ANJAL_MODULEDIR,
		"online", online,
		NULL);

	g_signal_connect (
		shell, "window-destroyed",
		G_CALLBACK (shell_window_destroyed_cb), NULL);

	g_object_unref (client);

	g_idle_add ((GSourceFunc) idle_cb, remaining_args);
}

int
main (int argc, char *argv[])
{
	GError *error = NULL;
	EShell *default_shell;
	GConfClient *client;

#ifdef G_OS_WIN32
	extern void link_shutdown (void);
	set_paths ();
#endif

	static GOptionEntry entries[] = {
		{ "windowed", 'w', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_NONE, &windowed,N_("Run Anjal in a window"), NULL },
		{ "default-mailer", 'd', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_NONE, &default_app,N_("Make Anjal the default email client"), NULL },
		{ G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_STRING_ARRAY, &remaining_args, NULL, NULL },
		{ NULL, 0, 0, 0, NULL, NULL, NULL }
	};

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	
	gtk_set_locale ();

#if HAVE_ANERLEY|HAVE_CLUTTER
	if (!gtk_clutter_init_with_args (&argc, &argv, _("Anjal email client"), entries, NULL, &error)) {
		g_error ("Unable to start Anjal: %s\n", error->message);
		g_error_free(error);
	}
#if HAVE_ANERLEY	
	nbtk_style_load_from_file (nbtk_style_get_default (), PACKAGE_DATA_DIR G_DIR_SEPARATOR_S "anerley" G_DIR_SEPARATOR_S "style.css", NULL);
#endif

#else
	if (!gtk_init_with_args (&argc, &argv, _("Anjal email client"), entries, NULL, &error)) {
		g_error ("Unable to start Anjal: %s\n", error->message);
		g_error_free(error);
	}
#endif	
	client = gconf_client_get_default();

	if (!g_thread_get_initialized ())
		g_thread_init (NULL);
	dbus_g_thread_init ();

	e_passwords_init();
	gtk_icon_theme_append_search_path (gtk_icon_theme_get_default(), PACKAGE_DATA_DIR G_DIR_SEPARATOR_S "anjal" G_DIR_SEPARATOR_S "icons");
	categories_icon_theme_hack ();

	gconf_client_set_bool (client, "/apps/evolution/mail/display/enable_vfolders", FALSE, NULL);
	g_object_unref (client);
	
#if 0
	app = unique_app_new ("org.gnome.Anjal", NULL);
	  if (unique_app_is_running (app))  {
		   gboolean cmd_line =  (remaining_args && remaining_args[0] != NULL);
		   
		  if (!cmd_line)
			unique_app_send_message (app, UNIQUE_ACTIVATE, NULL);
		  else {
			  UniqueMessageData *data = unique_message_data_new ();
			  unique_message_data_set_text (data, remaining_args[0], -1);
			  unique_app_send_message (app, UNIQUE_OPEN, data);
			  unique_message_data_free (data);
		  }

		  return 0;
	  }
#endif	  
	/* Let Evo's libs load anjal as the default backend. */  
	mail_mt_set_backend ("anjal");
	create_default_shell ();

	if (windowed)
		anjal_icon_decoration = TRUE;


	
	gtk_main ();

	default_shell = e_shell_get_default ();
	g_object_unref (default_shell);
	if (E_IS_SHELL (default_shell))
		g_warning ("Shell not finalized on exit");
	
#ifdef G_OS_WIN32
	link_shutdown ();
#endif
	return 0;
}

#ifndef HACK
gpointer em_format_html_print_new(gpointer p1 G_GNUC_UNUSED,
				  int i G_GNUC_UNUSED)
{
	return NULL;
}

void em_format_html_print_raw_message (gpointer p1 G_GNUC_UNUSED,
				       gpointer p2 G_GNUC_UNUSED)
{
	return ;
}

void 
eab_merging_book_commit_contact ()
{
}

void 
e_searching_tokenizer_set_primary_case_sensitivity ()
{
}

void 
eab_prompt_save_dialog ()
{
}

void
eab_merging_book_add_contact ()
{
}

void
e_searching_tokenizer_match_count ()
{
}

void
addressbook_load_cancel()
{
}

void
e_searching_tokenizer_new ()
{
}

void
e_searching_tokenizer_set_primary_search_string()
{
}

void
addressbook_load()
{
}

void
eab_load_error_dialog()
{
}

#endif
