/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _custom_cell_renderer_vbox_included_
#define _custom_cell_renderer_vbox_included_

#include <gtk/gtk.h>

/* Some boilerplate GObject type check and type cast macros.
 *  'klass' is used here instead of 'class', because 'class'
 *  is a c++ keyword */

#define CUSTOM_TYPE_CELL_RENDERER_VBOX             (custom_cell_renderer_vbox_get_type())
#define CUSTOM_CELL_RENDERER_VBOX(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj),  CUSTOM_TYPE_CELL_RENDERER_VBOX, CustomCellRendererVBox))
#define CUSTOM_CELL_RENDERER_VBOX_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass),  CUSTOM_TYPE_CELL_RENDERER_VBOX, CustomCellRendererVBoxClass))
#define CUSTOM_IS_CELL_RENDERER_VBOX(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CUSTOM_TYPE_CELL_RENDERER_VBOX))
#define CUSTOM_IS_CELL_RENDERER_VBOX_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass),  CUSTOM_TYPE_CELL_RENDERER_VBOX))
#define CUSTOM_CELL_RENDERER_VBOX_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj),  CUSTOM_TYPE_CELL_RENDERER_VBOX, CustomCellRendererVBoxClass))

typedef struct _CustomCellRendererVBox CustomCellRendererVBox;
typedef struct _CustomCellRendererVBoxClass CustomCellRendererVBoxClass;

/* CustomCellRendererVBox: Our custom cell renderer
 *   structure. Extend according to need */
typedef  gboolean (*CellRenderActivateFunc)  (GtkCellRenderer *cell, GdkEvent *event,GtkWidget *widget,const gchar *path,const GdkRectangle *background_area,const GdkRectangle *cell_area,GtkCellRendererState flags);

struct _CustomCellRendererVBox
{
  GtkCellRenderer   parent;

  GList *children;
};


struct _CustomCellRendererVBoxClass
{
  GtkCellRendererClass  parent_class;
};


GType                custom_cell_renderer_vbox_get_type (void);

GtkCellRenderer     *custom_cell_renderer_vbox_new (void);
void		     custom_cell_renderer_vbox_append (GtkCellRenderer *cell, GtkCellRenderer *child);

#endif /* _custom_cell_renderer_vbox_included_ */

