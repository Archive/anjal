/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_FOLDER_VIEW_H_
#define _MAIL_FOLDER_VIEW_H_

#include <gtk/gtk.h>
#include <camel/camel.h>
#include "mail-conv-view.h"

#define MAIL_FOLDER_VIEW_TYPE        (mail_folder_view_get_type ())
#define MAIL_FOLDER_VIEW(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), MAIL_FOLDER_VIEW_TYPE, MailFolderView))
#define MAIL_FOLDER_VIEW_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), MAIL_FOLDER_VIEW_TYPE, MailFolderViewClass))
#define IS_MAIL_FOLDER_VIEW(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAIL_FOLDER_VIEW_TYPE))
#define IS_MAIL_FOLDER_VIEW_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), MAIL_FOLDER_VIEW_TYPE))
#define MAIL_FOLDER_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), MAIL_FOLDER_VIEW_TYPE, MailFolderViewClass))

typedef struct _MailFolderViewPrivate MailFolderViewPrivate;

typedef struct _MailFolderView {
	GtkVBox parent;
	 int type;
	 char *uri;
	 MailViewFlags flags;
	
	/* Base class ends */
	GtkTreeView *tree;
	GtkTreeModel *model;
	GtkTreeView *view;
	
	MailConvView *mail;
	GtkWidget *slidebox;
	GtkWidget *slider;

	GtkWidget *tab_label;
	GtkWidget *menu_label;
	
	MailFolderViewPrivate *priv;
	char *search;
	char *search_str;
	char *search_state;

	int x_pos;
	int y_pos;
} MailFolderView;

typedef struct _MailFolderViewClass {
	GtkVBoxClass parent_class;
 
	void (*message_shown) (MailConvView *);
	void (*view_close) (MailConvView *);
	void (*message_new) (MailConvView *, gpointer, gpointer);
	void (*search_set) (MailFolderView *);
	void (*view_loaded) (MailFolderView *);	
} MailFolderViewClass;

MailFolderView * mail_folder_view_new (void);

void mail_folder_view_set_folder (MailFolderView *mfv, CamelFolder *folder, const char *uri);
void mail_folder_view_set_folder_uri (MailFolderView *mfv, const char *uri);
void mail_folder_view_set_message (MailFolderView *mfv, const char *uid, gboolean nomarkseen);
void mail_folder_view_show_sort_popup  (MailFolderView *, GtkWidget *);
void mail_folder_view_show_list (MailFolderView *mfv);
void mail_folder_view_activate (MailFolderView *mfv, GtkWidget *tree, GtkWidget *folder_tree, GtkWidget *check_mail, GtkWidget *sort_by, GtkWidget *slider, gboolean act);
void mail_folder_view_check_mail (MailFolderView *mfv);
void mail_folder_view_save (MailFolderView *mfv);
void mail_folder_view_set_search (MailFolderView *mfv, const char *search, const char *str);
void mail_folder_view_set_folder_pane (MailFolderView *mfv, GtkWidget *pane);
#endif
