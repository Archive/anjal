/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _custom_cell_renderer_hbox_included_
#define _custom_cell_renderer_hbox_included_

#include <gtk/gtk.h>

/* Some boilerplate GObject type check and type cast macros.
 *  'klass' is used here instead of 'class', because 'class'
 *  is a c++ keyword */

#define CUSTOM_TYPE_CELL_RENDERER_HBOX             (custom_cell_renderer_hbox_get_type())
#define CUSTOM_CELL_RENDERER_HBOX(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj),  CUSTOM_TYPE_CELL_RENDERER_HBOX, CustomCellRendererHBox))
#define CUSTOM_CELL_RENDERER_HBOX_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass),  CUSTOM_TYPE_CELL_RENDERER_HBOX, CustomCellRendererHBoxClass))
#define CUSTOM_IS_CELL_RENDERER_HBOX(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CUSTOM_TYPE_CELL_RENDERER_HBOX))
#define CUSTOM_IS_CELL_RENDERER_HBOX_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass),  CUSTOM_TYPE_CELL_RENDERER_HBOX))
#define CUSTOM_CELL_RENDERER_HBOX_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj),  CUSTOM_TYPE_CELL_RENDERER_HBOX, CustomCellRendererHBoxClass))

typedef struct _CustomCellRendererHBox CustomCellRendererHBox;
typedef struct _CustomCellRendererHBoxClass CustomCellRendererHBoxClass;

/* CustomCellRendererHBox: Our custom cell renderer
 *   structure. Extend according to need */

struct _CustomCellRendererHBox
{
  GtkCellRenderer   parent;

  GList *start_children;
  GList *end_children;
};


struct _CustomCellRendererHBoxClass
{
  GtkCellRendererClass  parent_class;
};

GType                custom_cell_renderer_hbox_get_type (void);

GtkCellRenderer     *custom_cell_renderer_hbox_new (void);
void		     custom_cell_renderer_hbox_pack_start (GtkCellRenderer *cell, GtkCellRenderer *child, gboolean expand);
void		     custom_cell_renderer_hbox_pack_end (GtkCellRenderer *cell, GtkCellRenderer *child, gboolean expand);

#endif /* _custom_cell_renderer_hbox_included_ */

