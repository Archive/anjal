/*
 * anjal-shell-sidebar.h
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 */

#ifndef ANJAL_SHELL_SIDEBAR_H
#define ANJAL_SHELL_SIDEBAR_H

#include <shell/e-shell-sidebar.h>
#include <shell/e-shell-view.h>
#include <mail/em-folder-tree.h>

/* Standard GObject macros */
#define ANJAL_TYPE_SHELL_SIDEBAR \
	(anjal_shell_sidebar_get_type ())
#define ANJAL_SHELL_SIDEBAR(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), ANJAL_TYPE_SHELL_SIDEBAR, AnjalShellSidebar))
#define ANJAL_SHELL_SIDEBAR_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), ANJAL_TYPE_SHELL_SIDEBAR, AnjalShellSidebarClass))
#define ANJAL_IS_SHELL_SIDEBAR(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), ANJAL_TYPE_SHELL_SIDEBAR))
#define ANJAL_IS_SHELL_SIDEBAR_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), ANJAL_TYPE_SHELL_SIDEBAR))
#define ANJAL_SHELL_SIDEBAR_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), ANJAL_TYPE_SHELL_SIDEBAR, AnjalShellSidebarClass))

G_BEGIN_DECLS

typedef struct _AnjalShellSidebar AnjalShellSidebar;
typedef struct _AnjalShellSidebarClass AnjalShellSidebarClass;
typedef struct _AnjalShellSidebarPrivate AnjalShellSidebarPrivate;

struct _AnjalShellSidebar {
	EShellSidebar parent;
	AnjalShellSidebarPrivate *priv;
};

struct _AnjalShellSidebarClass {
	EShellSidebarClass parent_class;
};

GType		anjal_shell_sidebar_get_type	(void);
void		anjal_shell_sidebar_register_type
					(GTypeModule *type_module);
GtkWidget *	anjal_shell_sidebar_new(EShellView *shell_view);
EMFolderTree *	anjal_shell_sidebar_get_folder_tree
					(AnjalShellSidebar *mail_shell_sidebar);

G_END_DECLS

#endif /* ANJAL_SHELL_SIDEBAR_H */
