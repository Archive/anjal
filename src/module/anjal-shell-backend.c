/*
 * anjal-shell-backend.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 */

#include "anjal-shell-backend.h"

#include <glib/gi18n.h>
#include <camel/camel-disco-store.h>
#include <camel/camel-offline-store.h>
#include <camel/camel-session.h>
#include <camel/camel-url.h>

#include "e-util/e-account-utils.h"
#include "e-util/e-binding.h"
#include "e-util/e-import.h"
#include "e-util/e-util.h"
#include "shell/e-shell.h"
#include "shell/e-shell-window.h"
#include "composer/e-msg-composer.h"

#include "e-mail-shell-settings.h"
#include "anjal-shell-sidebar.h"
#include "anjal-shell-view.h"

#include <mail/e-mail-browser.h>
#include <mail/e-mail-local.h>
#include <mail/e-mail-reader.h>
#include <mail/e-mail-store.h>
#include <mail/em-account-editor.h>
/*#include <mail/em-account-prefs.h>*/
/*#include <mail/em-composer-prefs.h>*/
#include <mail/em-composer-utils.h>
#include <mail/em-folder-utils.h>
#include <mail/em-format-hook.h>
#include <mail/em-format-html-display.h>
/*#include <mail/em-mailer-prefs.h>*/
/*#include <mail/em-network-prefs.h>*/
#include <mail/em-utils.h>
#include <mail/mail-config.h>
#include <mail/mail-ops.h>
#include <mail/mail-send-recv.h>
#include <mail/mail-session.h>
#include <mail/mail-vfolder.h>
/*#include <mail/importers/mail-importer.h>*/

#define ANJAL_SHELL_BACKEND_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE \
	((obj), ANJAL_TYPE_SHELL_BACKEND, AnjalShellBackendPrivate))

#define BACKEND_NAME "anjal"

struct _AnjalShellBackendPrivate {
	gint mail_sync_in_progress;
	guint mail_sync_source_id;
};

static gpointer parent_class;
static GType anjal_shell_backend_type;

EImportImporter *mbox_importer_peek(void);
EImportImporter *elm_importer_peek(void);
EImportImporter *pine_importer_peek(void);

static void
anjal_shell_backend_init_importers (void)
{
	EImportClass *import_class;
	EImportImporter *importer;

	import_class = g_type_class_ref (e_import_get_type ());

	importer = mbox_importer_peek ();
	e_import_class_add_importer (import_class, importer, NULL, NULL);

	importer = elm_importer_peek ();
	e_import_class_add_importer (import_class, importer, NULL, NULL);

	importer = pine_importer_peek ();
	e_import_class_add_importer (import_class, importer, NULL, NULL);
}

static void
anjal_shell_backend_sync_store_done_cb (CamelStore *store,
                                        gpointer user_data)
{
	AnjalShellBackend *anjal_shell_backend = user_data;

	anjal_shell_backend->priv->mail_sync_in_progress--;
}

static void
anjal_shell_backend_sync_store_cb (CamelStore *store,
                                   AnjalShellBackend *anjal_shell_backend)
{
	anjal_shell_backend->priv->mail_sync_in_progress++;

	mail_sync_store (
		store, FALSE,
		anjal_shell_backend_sync_store_done_cb,
		anjal_shell_backend);
}

static gboolean
anjal_shell_backend_mail_sync (AnjalShellBackend *anjal_shell_backend)
{
	EShell *shell;
	EShellBackend *shell_backend;

	shell_backend = E_SHELL_BACKEND (anjal_shell_backend);
	shell = e_shell_backend_get_shell (shell_backend);

	/* Obviously we can only sync in online mode. */
	if (!e_shell_get_online (shell))
		goto exit;

	/* If a sync is still in progress, skip this round. */
	if (anjal_shell_backend->priv->mail_sync_in_progress)
		goto exit;

	e_mail_store_foreach (
		(GHFunc) anjal_shell_backend_sync_store_cb,
		anjal_shell_backend);

exit:
	return TRUE;
}

static void
anjal_shell_backend_handle_email_uri_cb (gchar *folder_uri,
                                         CamelFolder *folder,
                                         gpointer user_data)
{
	EShellBackend *shell_backend = user_data;
	CamelURL *url = user_data;
	const gchar *forward;
	const gchar *reply;
	const gchar *uid;

	if (folder == NULL) {
		g_warning ("Could not open folder '%s'", folder_uri);
		goto exit;
	}

	forward = camel_url_get_param (url, "forward");
	reply = camel_url_get_param (url, "reply");
	uid = camel_url_get_param (url, "uid");

	if (reply != NULL) {
		gint mode;

		if (g_strcmp0 (reply, "all") == 0)
			mode = REPLY_MODE_ALL;
		else if (g_strcmp0 (reply, "list") == 0)
			mode = REPLY_MODE_LIST;
		else
			mode = REPLY_MODE_SENDER;

		em_utils_reply_to_message (folder, uid, NULL, mode, NULL);

	} else if (forward != NULL) {
		GPtrArray *uids;

		uids = g_ptr_array_new ();
		g_ptr_array_add (uids, g_strdup (uid));

		if (g_strcmp0 (forward, "attached") == 0)
			em_utils_forward_attached (folder, uids, folder_uri);
		else if (g_strcmp0 (forward, "inline") == 0)
			em_utils_forward_inline (folder, uids, folder_uri);
		else if (g_strcmp0 (forward, "quoted") == 0)
			em_utils_forward_quoted (folder, uids, folder_uri);
		else
			em_utils_forward_messages (folder, uids, folder_uri);

	} else {
		GtkWidget *browser;

		/* FIXME Should pass in the shell module. */
		browser = e_mail_browser_new (shell_backend);
		e_mail_reader_set_folder (
			E_MAIL_READER (browser), folder, folder_uri);
		e_mail_reader_set_message (E_MAIL_READER (browser), uid);
		gtk_widget_show (browser);
	}

exit:
	camel_url_free (url);
}

static gboolean
anjal_shell_backend_handle_uri_cb (EShell *shell,
                                   const gchar *uri,
                                   AnjalShellBackend *anjal_shell_backend)
{
	gboolean handled = TRUE;

	if (g_str_has_prefix (uri, "mailto:")) {
		if (em_utils_check_user_can_send_mail ())
			em_utils_compose_new_message_with_mailto (uri, NULL);

	} else if (g_str_has_prefix (uri, "email:")) {
		CamelURL *url;

		url = camel_url_new (uri, NULL);
		if (camel_url_get_param (url, "uid") != NULL) {
			gchar *curi = em_uri_to_camel (uri);

			mail_get_folder (
				curi, 0,
				anjal_shell_backend_handle_email_uri_cb,
				anjal_shell_backend, mail_msg_unordered_push);
			g_free (curi);

		} else {
			g_warning ("Email URI's must include a uid parameter");
			camel_url_free (url);
		}
	} else
		handled = FALSE;

	return handled;
}

static void
anjal_shell_backend_prepare_for_quit_cb (EShell *shell,
                                         EActivity *activity,
                                         EShellBackend *shell_backend)
{
	AnjalShellBackendPrivate *priv;

	priv = ANJAL_SHELL_BACKEND_GET_PRIVATE (shell_backend);

	/* Prevent a sync from starting while trying to shutdown. */
	if (priv->mail_sync_source_id > 0) {
		g_source_remove (priv->mail_sync_source_id);
		priv->mail_sync_source_id = 0;
	}
}

static void
anjal_shell_backend_send_receive_cb (EShell *shell,
                                     GtkWindow *parent,
                                     EShellBackend *shell_backend)
{
	em_utils_clear_get_password_canceled_accounts_flag ();
	mail_send_receive (parent);
}

static void
anjal_shell_backend_window_created_cb (EShell *shell,
                                       GtkWindow *window,
                                       EShellBackend *shell_backend)
{
	EShellSettings *shell_settings;
	static gboolean first_time = TRUE;

	shell_settings = e_shell_get_shell_settings (shell);

	/* This applies to both the composer and signature editor. */
	if (GTKHTML_IS_EDITOR (window)) {
		GList *spell_languages;

		e_binding_new (
			shell_settings, "composer-inline-spelling",
			window, "inline-spelling");

		e_binding_new (
			shell_settings, "composer-magic-links",
			window, "magic-links");

		e_binding_new (
			shell_settings, "composer-magic-smileys",
			window, "magic-smileys");

		spell_languages = e_load_spell_languages ();
		gtkhtml_editor_set_spell_languages (
			GTKHTML_EDITOR (window), spell_languages);
		g_list_free (spell_languages);
	}

	if (E_IS_MSG_COMPOSER (window)) {
		/* Integrate the new composer into the mail module. */
		em_configure_new_composer (E_MSG_COMPOSER (window));
		return;
	}

	if (!E_IS_SHELL_WINDOW (window))
		return;

	if (first_time) {
		g_signal_connect (
			window, "map-event",
			G_CALLBACK (e_msg_composer_check_autosave), NULL);
		first_time = FALSE;
	}
}
static void
anjal_shell_backend_constructed (GObject *object)
{
	AnjalShellBackendPrivate *priv;
	EShell *shell;
	EShellBackend *shell_backend;

	priv = ANJAL_SHELL_BACKEND_GET_PRIVATE (object);

	shell_backend = E_SHELL_BACKEND (object);
	shell = e_shell_backend_get_shell (shell_backend);

	/* This also initializes Camel, so it needs to happen early. */
	mail_session_init ();

	/* Register format types for EMFormatHook. */
	em_format_hook_register_type (em_format_get_type ());

	/* Register plugin hook types. */
	em_format_hook_get_type ();

	anjal_shell_backend_init_importers ();

	g_signal_connect (
		shell, "handle-uri",
		G_CALLBACK (anjal_shell_backend_handle_uri_cb),
		shell_backend);

	g_signal_connect (
		shell, "prepare-for-quit",
		G_CALLBACK (anjal_shell_backend_prepare_for_quit_cb),
		shell_backend);

	g_signal_connect (
		shell, "send-receive",
		G_CALLBACK (anjal_shell_backend_send_receive_cb),
		shell_backend);

	g_signal_connect (
		shell, "window-created",
		G_CALLBACK (anjal_shell_backend_window_created_cb),
		shell_backend);

	mail_config_init ();
	mail_msg_init ();

	e_mail_store_init (mail_session_get_data_dir ());

	e_mail_shell_settings_init (shell);
}

static void
anjal_shell_backend_start (EShellBackend *shell_backend)
{
	AnjalShellBackendPrivate *priv;
	EShell *shell;
	EShellSettings *shell_settings;
	gboolean enable_search_folders;

	priv = ANJAL_SHELL_BACKEND_GET_PRIVATE (shell_backend);

	shell = e_shell_backend_get_shell (shell_backend);
	shell_settings = e_shell_get_shell_settings (shell);

	/* XXX Do we really still need this flag? */
	//mail_session_set_interactive (TRUE);

	enable_search_folders = e_shell_settings_get_boolean (
		shell_settings, "mail-enable-search-folders");
	if (enable_search_folders)
		vfolder_load_storage ();

	mail_autoreceive_init (shell_backend, session);

	if (g_getenv ("CAMEL_FLUSH_CHANGES") != NULL)
		priv->mail_sync_source_id = g_timeout_add_seconds (
			mail_config_get_sync_timeout (),
			(GSourceFunc) anjal_shell_backend_mail_sync,
			shell_backend);
}

static void
anjal_shell_backend_class_init (AnjalShellBackendClass *class)
{
	GObjectClass *object_class;
	EShellBackendClass *shell_backend_class;

	parent_class = g_type_class_peek_parent (class);
	g_type_class_add_private (class, sizeof (AnjalShellBackendPrivate));

	object_class = G_OBJECT_CLASS (class);
	object_class->constructed = anjal_shell_backend_constructed;

	shell_backend_class = E_SHELL_BACKEND_CLASS (class);
	shell_backend_class->shell_view_type = ANJAL_TYPE_SHELL_VIEW;
	shell_backend_class->name = BACKEND_NAME;
	shell_backend_class->aliases = "";
	shell_backend_class->schemes = "mailto:email";
	shell_backend_class->sort_order = 200;
	shell_backend_class->preferences_page = "mail-accounts";
	shell_backend_class->start = anjal_shell_backend_start;
}

static void
anjal_shell_backend_init (AnjalShellBackend *mail_shell_backend)
{
	mail_shell_backend->priv =
		ANJAL_SHELL_BACKEND_GET_PRIVATE (mail_shell_backend);
}

GType
anjal_shell_backend_get_type (void)
{
	return anjal_shell_backend_type;
}

void
anjal_shell_backend_register_type (GTypeModule *type_module)
{
	const GTypeInfo type_info = {
		sizeof (AnjalShellBackendClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) anjal_shell_backend_class_init,
		(GClassFinalizeFunc) NULL,
		NULL,  /* class_data */
		sizeof (AnjalShellBackend),
		0,     /* n_preallocs */
		(GInstanceInitFunc) anjal_shell_backend_init,
		NULL   /* value_table */
	};

	anjal_shell_backend_type = g_type_module_register_type (
		type_module, E_TYPE_MAIL_BACKEND,
		"AnjalShellBackend", &type_info, 0);
}

/******************* Code below here belongs elsewhere. *******************/

#include "filter/e-filter-option.h"
#include "shell/e-shell-settings.h"
#include "mail/e-mail-label-list-store.h"

GSList *
e_mail_labels_get_filter_options (void)
{
	EShell *shell;
	EShellSettings *shell_settings;
	EMailLabelListStore *list_store;
	GtkTreeModel *model;
	GtkTreeIter iter;
	GSList *list = NULL;
	gboolean valid;

	shell = e_shell_get_default ();
	shell_settings = e_shell_get_shell_settings (shell);
	list_store = e_shell_settings_get_object (
		shell_settings, "mail-label-list-store");

	model = GTK_TREE_MODEL (list_store);
	valid = gtk_tree_model_get_iter_first (model, &iter);

	while (valid) {
		struct _filter_option *option;
		gchar *name, *tag;

		name = e_mail_label_list_store_get_name (list_store, &iter);
		tag = e_mail_label_list_store_get_tag (list_store, &iter);

		option = g_new0 (struct _filter_option, 1);
		option->title = e_str_without_underscores (name);
		option->value = tag;  /* takes ownership */
		list = g_slist_prepend (list, option);

		g_free (name);

		valid = gtk_tree_model_iter_next (model, &iter);
	}

	g_object_unref (list_store);

	return g_slist_reverse (list);
}

