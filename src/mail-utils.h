/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#include <gtk/gtk.h>

#ifndef MAIL_UTILS_H
#define MAIL_UTILS_H

GdkPixbuf * mail_utils_get_icon (const char *icon, int icon_size);
char * mail_utils_get_icon_path (const char *icon, int icon_size);
void mail_util_clear_photo_cache (void);
int mail_util_fill_photo (GtkWidget *widget, const char *address);

#endif
