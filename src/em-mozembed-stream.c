/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <gtk/gtk.h>
#include "em-mozembed-stream.h"

#define d(x)

static void em_mozembed_stream_class_init (EMMozEmbedStreamClass *klass);
static void em_mozembed_stream_init (CamelObject *object);
static void em_mozembed_stream_finalize (CamelObject *object);

static ssize_t emms_sync_write(CamelStream *stream, const char *buffer, size_t n);
static int emms_sync_close(CamelStream *stream);
static int emms_sync_flush(CamelStream *stream);

static EMSyncStreamClass *parent_class = NULL;

CamelType
em_mozembed_stream_get_type (void)
{
	static CamelType type = CAMEL_INVALID_TYPE;

	if (type == CAMEL_INVALID_TYPE) {
		parent_class = (EMSyncStreamClass *)em_sync_stream_get_type();
		type = camel_type_register (em_sync_stream_get_type(),
					    "EMMozEmbedStream",
					    sizeof (EMMozEmbedStream),
					    sizeof (EMMozEmbedStreamClass),
					    (CamelObjectClassInitFunc) em_mozembed_stream_class_init,
					    NULL,
					    (CamelObjectInitFunc) em_mozembed_stream_init,
					    (CamelObjectFinalizeFunc) em_mozembed_stream_finalize);
	}

	return type;
}

static void
em_mozembed_stream_class_init (EMMozEmbedStreamClass *klass)
{
	((EMSyncStreamClass *)klass)->sync_write = emms_sync_write;
	((EMSyncStreamClass *)klass)->sync_flush = emms_sync_flush;
	((EMSyncStreamClass *)klass)->sync_close = emms_sync_close;
}

static void
em_mozembed_stream_init (CamelObject *object)
{
	EMMozEmbedStream *emms = (EMMozEmbedStream *)object;

	emms->open = FALSE;
}

static void
emms_cleanup(EMMozEmbedStream *emms)
{
	emms->sync.cancel = TRUE;	
}

static void
em_mozembed_stream_finalize (CamelObject *object)
{
	EMMozEmbedStream *emms = (EMMozEmbedStream *)object;

	camel_stream_close((CamelStream *)emms);
}

static ssize_t
emms_sync_write(CamelStream *stream, const char *buffer, size_t n)
{
	EMMozEmbedStream *emms = EM_MOZEMBED_STREAM (stream);

	gtk_moz_embed_append_data (emms->view, buffer, n); 
	return (ssize_t) n;
}

static int
emms_sync_flush(CamelStream *stream)
{
	EMMozEmbedStream *emms = (EMMozEmbedStream *)stream;

	return 0;
}

static int
emms_sync_close(CamelStream *stream)
{
	EMMozEmbedStream *emms = (EMMozEmbedStream *)stream;
	
	if (emms->open) {
		emms->open = FALSE;
		gtk_moz_embed_close_stream (emms->view);
		gtk_moz_embed_reload (emms->view, GTK_MOZ_EMBED_FLAG_RELOADNORMAL);
		gtk_widget_set_size_request (emms->view, -1, gtk_moz_embed_get_page_height(emms->view));
	}
	return 0;
}

CamelStream *
em_mozembed_stream_new(GtkMozEmbed *view, GtkWidget *body)
{
	EMMozEmbedStream *new;

	new = EM_MOZEMBED_STREAM (camel_object_new (EM_MOZEMBED_STREAM_TYPE));
	new->flags = 0;
	new->view = view;
	new->body = body;
	gtk_moz_embed_open_stream (view, "file:///", "text/html");
	new->open = TRUE;
	em_sync_stream_set_buffer_size(&new->sync, 8192);

	return (CamelStream *)new;
}

void
em_mozembed_stream_set_flags (EMMozEmbedStream *emms, int flags)
{
	emms->flags = flags;
}

GtkWidget *
em_mozembed_stream_get_body (EMMozEmbedStream *emms)
{
	return emms->body;
}
