/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_PEOPLE_VIEW_H_
#define _MAIL_PEOPLE_VIEW_H_

#include <gtk/gtk.h>
#include <webkit/webkit.h>
#include "mail-view.h"

#define MAIL_PEOPLE_VIEW_TYPE        (mail_people_view_get_type ())
#define MAIL_PEOPLE_VIEW(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), MAIL_PEOPLE_VIEW_TYPE, MailFolderView))
#define MAIL_PEOPLE_VIEW_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), MAIL_PEOPLE_VIEW_TYPE, MailFolderViewClass))
#define IS_MAIL_PEOPLE_VIEW(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAIL_PEOPLE_VIEW_TYPE))
#define IS_MAIL_PEOPLE_VIEW_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), MAIL_PEOPLE_VIEW_TYPE))
#define MAIL_PEOPLE_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), MAIL_PEOPLE_VIEW_TYPE, MailFolderViewClass))


typedef struct _MailPeopleViewPrivate MailPeopleViewPrivate;

typedef struct _MailPeopleView {
	GtkVBox parent;
	int type;
	char *uri;
	MailViewFlags flags;
	/* Base class of MailChildView ends */

	MailPeopleViewPrivate *priv;
} MailPeopleView;

typedef struct _MailPeopleViewClass {
	GtkVBoxClass parent_class;

	void (* view_close) (MailPeopleView *);	
} MailPeopleViewClass;

MailPeopleView *mail_people_view_new (void);
GtkWidget * mail_people_view_get_tab_widget(MailPeopleView *mcv);
void mail_people_view_activate (MailPeopleView *mcv, GtkWidget *tree, GtkWidget *folder_tree, GtkWidget *check_mail, GtkWidget *sort_by, GtkWidget *slider, gboolean act);
#endif
