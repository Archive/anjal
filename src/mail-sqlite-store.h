/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_SQLITE_STORE
#define _MAIL_SQLITE_STORE

#include <gtk/gtk.h>
#include <camel/camel.h>

#define MAIL_TYPE_SQLITE_STORE             (mail_sqlite_store_get_type())
#define MAIL_SQLITE_STORE(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), MAIL_TYPE_SQLITE_STORE, MailSqliteStore))
#define MAIL_SQLITE_STORE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), MAIL_TYPE_SQLITE_STORE,GObject))
#define MAIL_IS_SQLITE_STORE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), MAIL_TYPE_SQLITE_STORE))
#define MAIL_IS_SQLITE_STORE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass),  MAIL_TYPE_SQLITE_STORE))
#define MAIL_SQLITE_STORE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), MAIL_TYPE_SQLITE_STORE, MailSqliteStoreClass))

typedef struct _MailSqliteStore MailSqliteStore;
typedef struct _MailSqliteStoreClass MailSqliteStoreClass;
typedef struct _MailSqliteStorePrivate MailSqliteStorePrivate;

struct _MailSqliteStore {
	 GObject parent;
	 gint    n_columns;
	 gint    stamp;

	MailSqliteStorePrivate *priv;
};

struct _MailSqliteStoreClass {
    GObjectClass parent_class;
};

typedef struct _MailSqliteRecord MailSqliteRecord;
struct _MailSqliteRecord {
	char *uid;
	char *from;
	char *subject;
	char *date;
	guint32 flags;
};


enum {
	COL_UID = 0,
	COL_FROM,
	COL_SUBJECT,
	COL_DATE,
	COL_FLAGS,
	COL_RECORD,
	NUM_COLS
};

GType           mail_sqlite_store_get_type    (void) G_GNUC_CONST;
GtkTreeModel*   mail_sqlite_store_new         (void);
void            mail_sqlite_store_set_folder (MailSqliteStore  *self, CamelFolder *folder, CamelException *ex);

#endif /* _MAIL_SQLITE_STORE */
