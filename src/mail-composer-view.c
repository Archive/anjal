/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Johnny Jacob <jjohnny@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <glib/gi18n.h>

#include <e-util/e-alert-dialog.h>

#include "mail-view.h"
#include "mail-composer-view.h"
#include <composer/e-composer-header-table.h>
#include <composer/e-msg-composer.h>
#include <composer/e-composer-actions.h>

struct  _MailComposerViewPrivate {
	GtkWidget *box;
	GtkWidget *composer;
	GList *children;
};

enum {
	 MESSAGE_SHOWN,
	VIEW_CLOSE,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (MailComposerView, mail_composer_view, GTK_TYPE_VBOX)

struct _EMsgComposer* em_utils_compose_lite_new_message (char *);

static void
mail_composer_view_init (MailComposerView  *shell)
{
	shell->priv = g_new0(MailComposerViewPrivate, 1);
	shell->uri = "new-message://";
	shell->type = MAIL_VIEW_COMPOSER;
}

static void
mail_composer_view_finalize (GObject *object)
{
	MailComposerView *mcv = (MailComposerView *)object;


	gtk_widget_destroy (mcv->priv->composer);
	g_free (mcv->priv);
	G_OBJECT_CLASS (mail_composer_view_parent_class)->finalize (object);
}

static void
mail_composer_view_class_init (MailComposerViewClass *klass)
{
	/*Solve this with proper OO.*/

	GObjectClass * object_class = G_OBJECT_CLASS (klass);

	signals[VIEW_CLOSE] =
		g_signal_new ("view-close",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MailComposerViewClass , view_close),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	signals[MESSAGE_SHOWN] =
		g_signal_new ("message-shown",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MailComposerViewClass , message_shown),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	
	mail_composer_view_parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = mail_composer_view_finalize;	
};


static gboolean
mcv_btn_expose (GtkWidget *w, GdkEventExpose *event, MailComposerView *mcv)
{
	GdkPixbuf *img = g_object_get_data ((GObject *)w, "pbuf");
	cairo_t *cr = gdk_cairo_create (w->window);
	cairo_save (cr);
	gdk_cairo_set_source_pixbuf (cr, img, event->area.x-5, event->area.y-4);
	cairo_paint(cr);
	cairo_restore(cr);
	cairo_destroy (cr);

	return TRUE;
}

static void
composer_close_cb (struct _EMsgComposer *composer, MailComposerView *mcv)
{
	/*Emit view-close signal.*/
	//g_object_unref (composer);
	g_signal_emit (mcv, signals[VIEW_CLOSE], 0);
}

static void
composer_send_close_cb (struct _EMsgComposer *composer, MailComposerView *mcv)
{
	/*Emit view-close signal.*/
	//g_object_unref (composer);

	/* Only close when the Sending was successful */
	if (e_msg_composer_get_mail_sent(composer))
		g_signal_emit (mcv, signals[VIEW_CLOSE], 0);
}

static gboolean
mcv_key_pressed (GtkWidget *w, GdkEventKey *event, MailComposerView *mcv)
{
	gboolean ctrl = event->state & GDK_CONTROL_MASK ? TRUE : FALSE;
	switch (event->keyval) {
	case GDK_w:
	case GDK_W:
	{
		if (!ctrl)
			return FALSE;
		g_signal_emit (mcv, signals[VIEW_CLOSE], 0);			

		return TRUE;
	}
	
	default:
		return FALSE;
	}

	return TRUE;	
}

void
mail_composer_view_construct (MailComposerView *shell, struct _EMsgComposer *new_composer)
{
	MailComposerViewPrivate *priv = shell->priv;
	struct _EMsgComposer *composer;
	struct _EComposerHeaderTable *table;
	GtkWidget *close_button, *w, *img, *box, *tmp;
	GdkPixbuf *icon;
	int x,y;
	
	/*Tab Label*/
	close_button = gtk_button_new ();
	icon = gtk_widget_render_icon ((GtkWidget *)shell, "gtk-close", GTK_ICON_SIZE_MENU, NULL);

	img = gtk_image_new_from_pixbuf (icon);
	gtk_button_set_image ((GtkButton *)close_button, img);
	gtk_widget_show_all (close_button);
	gtk_button_set_relief((GtkButton *)close_button, GTK_RELIEF_NONE);
	gtk_button_set_focus_on_click ((GtkButton *)close_button, FALSE);
	gtk_widget_set_tooltip_text (close_button,  _("Close Tab"));\
	g_signal_connect (close_button, "clicked", G_CALLBACK(composer_close_cb), shell);
	
	gtk_icon_size_lookup_for_settings (gtk_widget_get_settings(close_button) , GTK_ICON_SIZE_MENU, &x, &y);
	gtk_widget_set_size_request (close_button, x+2, y+2);
	
	g_object_set_data ((GObject *)img, "pbuf", icon);
	g_signal_connect (img, "expose-event", G_CALLBACK(mcv_btn_expose), shell);
	g_signal_connect (shell, "key-press-event", G_CALLBACK(mcv_key_pressed), shell);

	shell->tab_label = gtk_hbox_new (FALSE, 0);
	w = gtk_label_new (_("New email"));
	gtk_box_pack_start ((GtkBox *)shell->tab_label, w, FALSE, FALSE, 0);
	gtk_box_pack_start ((GtkBox *)shell->tab_label, close_button, FALSE, FALSE, 0);
	gtk_widget_show_all (shell->tab_label);

	box = gtk_vbox_new (FALSE, 0);
	gtk_widget_show(box);
	if (new_composer)
		composer = new_composer;
	else
		composer = em_utils_compose_lite_new_message (NULL);

	table = e_msg_composer_get_header_table (composer);	
	e_composer_header_table_set_header_visible (table, E_COMPOSER_HEADER_FROM, FALSE);
	/* steal the composer's vbox */
	tmp = g_object_get_data((GObject *)composer, "vbox");
	gtk_widget_reparent (tmp, box);
	priv->box = box;
	gtk_box_pack_start ((GtkBox *)shell, box, TRUE, TRUE, 0);

	g_signal_connect (composer, "send", G_CALLBACK(composer_send_close_cb), shell);
	priv->composer = (GtkWidget *)composer;
	gtk_widget_show (tmp);

	gtk_widget_show ((GtkWidget *)shell);
}

void
mail_composer_view_replace_composer (MailComposerView *mcv, GtkWidget *composer)
{
	GtkWidget *w;

	w = g_object_get_data((GObject *)mcv->priv->composer, "vbox");
	gtk_widget_destroy (w);
	g_signal_handlers_disconnect_by_func (mcv->priv->composer, composer_close_cb, mcv);
	gtk_widget_destroy (mcv->priv->composer);
	mcv->priv->composer = composer;
	w = g_object_get_data((GObject *)mcv->priv->composer, "vbox");
	gtk_widget_reparent (w, mcv->priv->box);
	gtk_widget_show (w);
	gtk_widget_show (mcv->priv->box);
	
	//g_signal_emit (mcv, signals[MESSAGE_SHOWN], 0);
	g_signal_connect (mcv->priv->composer, "send", G_CALLBACK(composer_close_cb), mcv);

}

MailComposerView *
mail_composer_view_new ()
{
	MailComposerView *composer = g_object_new (MAIL_COMPOSER_VIEW_TYPE, NULL);
	mail_composer_view_construct (composer, NULL);

	return composer;
}

MailComposerView *
mail_composer_view_new_with_composer (GtkWidget *msgcomposer)
{
	MailComposerView *composer = g_object_new (MAIL_COMPOSER_VIEW_TYPE, NULL);
	mail_composer_view_construct (composer, (struct _EMsgComposer *)msgcomposer);

	return composer;
}

void
mail_composer_view_activate (MailComposerView *mfv, GtkWidget *folder_tree, GtkWidget *check_mail, GtkWidget *sort_by, gboolean act)
{
	 if (!check_mail || !sort_by)
		  return;
	 //gtk_widget_hide (folder_tree);
	 //g_signal_emit (mfv, signals[MESSAGE_SHOWN], 0);
	 gtk_widget_set_sensitive (check_mail, TRUE);
	 gtk_widget_set_sensitive (sort_by, FALSE);
}

/* Whether we can close the composer or not.

   The composer may be closed in two ways: by clicking the Send button
   or closing the tab. When the Send button is clicked, this function
   will only be called when the sending was successful (the
   e_msg_composer_get_mail_sent() is check in callback function).  When
   the tab is being closed, alert the user if editor is changed.
*/
gboolean
mail_composer_view_can_quit (MailComposerView *mcv)
{
	GtkhtmlEditor *editor;	
	struct _EMsgComposer *composer = (struct _EMsgComposer *)mcv->priv->composer;
	gint response;

	editor = GTKHTML_EDITOR(composer);
	
	if (e_msg_composer_get_mail_sent(composer))
		return TRUE;

	if (gtkhtml_editor_get_changed(editor) && !e_msg_composer_is_exiting (composer)) {
		const char *subject;
		EComposerHeaderTable *table = e_msg_composer_get_header_table (composer);

		subject = e_composer_header_table_get_subject (table);
		if (subject == NULL || *subject == '\0')
			subject = _("Untitled Message");
		response = e_alert_run_dialog_for_args (GTK_WINDOW (composer), "mail-composer:exit-unsaved", subject, NULL);
		if (response ==  GTK_RESPONSE_YES) {
			e_msg_composer_request_close (composer);
			gtk_action_activate (gtkhtml_editor_get_action (editor, "save-draft"));
		} else if (response == GTK_RESPONSE_CANCEL)
			return FALSE;
			
	}

	return TRUE;
}

#ifndef HACK_TO_COMPILE

void
em_composer_prefs_new_signature (GtkWindow *parent,
                                 gboolean html_mode)
{
}

void
e_contact_editor_new ()
{
}

void 
e_contact_list_editor_new ()
{
}
#endif
