/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_MESSAGE_VIEW_H_
#define _MAIL_MESSAGE_VIEW_H_

#include <gtk/gtk.h>
#include "mail-view.h"
#include <camel/camel-stream.h>

#define MAIL_MESSAGE_VIEW_TYPE        (mail_message_view_get_type ())
#define MAIL_MESSAGE_VIEW(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), MAIL_MESSAGE_VIEW_TYPE, MailFolderView))
#define MAIL_MESSAGE_VIEW_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), MAIL_MESSAGE_VIEW_TYPE, MailFolderViewClass))
#define IS_MAIL_MESSAGE_VIEW(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAIL_MESSAGE_VIEW_TYPE))
#define IS_MAIL_MESSAGE_VIEW_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), MAIL_MESSAGE_VIEW_TYPE))
#define MAIL_MESSAGE_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), MAIL_MESSAGE_VIEW_TYPE, MailFolderViewClass))

typedef struct _MailMessageViewPrivate MailMessageViewPrivate;

typedef struct _MailMessageView {
	GtkVBox parent;
	int type;
	char *uri;
	MailViewFlags flags;
	 
	GtkWidget *short_header;
	GtkWidget *details_event;
	GtkWidget *more_details;
	GtkWidget *sub_details;
	GtkWidget *date_details;
	GtkWidget *main_header;
	GtkWidget *body;
	GtkWidget *attachments;
	GtkWidget *footer;
	GtkWidget *frame;
	GtkWidget *cbox;
	GtkWidget *comp_view;
	GtkWidget *composer;
	GtkWidget *discard;
	GtkWidget *pop_out;

	MailMessageViewPrivate *priv;
	GtkWidget *efwd;
} MailMessageView;

typedef struct _MailMessageViewClass {
	GtkVBoxClass parent_class;

	void (*message_rendered) (MailMessageView *); 
	void (*message_selected) (MailMessageView *);
	void (*message_removed) (MailMessageView *);
	void (*message_loaded) (MailMessageView *);
	void (*message_reply) (MailMessageView *);

} MailMessageViewClass;

MailMessageView * mail_message_view_new (void);
void mail_message_view_set_message (MailMessageView *mmview, struct _CamelFolder *folder,const char *uri, const char *uid, gboolean show_composer);
GtkWidget * mail_message_view_create_webview (MailMessageView *mmv, GtkWidget *box);
CamelStream * mail_message_view_create_webstream (gpointer web, GtkWidget *w);
GtkWidget * mail_message_view_get_focus_widget (MailMessageView *mmv);
gboolean mail_message_view_get_unread (MailMessageView *mmv);
void mail_message_view_set_web_flags (gpointer web, int flags);
GtkWidget * mail_message_view_get_body (CamelStream *stream);
void mail_message_view_show_message (MailMessageView *mmv, gboolean show_composer);
#endif
