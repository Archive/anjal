/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_SEARCH_H_
#define _MAIL_SEARCH_H_

#include <gtk/gtk.h>
#include "mail-component.h"
#include "mail-view.h"
#include "mail-shell.h"

#define MAIL_SEARCH_TYPE        (mail_search_get_type ())
#define MAIL_SEARCH(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), MAIL_SEARCH_TYPE, MailSearch))
#define MAIL_SEARCH_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), MAIL_SEARCH_TYPE, MailSearchClass))
#define IS_MAIL_SEARCH(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAIL_SEARCH_TYPE))
#define IS_MAIL_SEARCH_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), MAIL_SEARCH_TYPE))
#define MAIL_SEARCH_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), MAIL_SEARCH_TYPE, MailSearchClass))

typedef struct _MailSearchPrivate MailSearchPrivate;

typedef struct _MailSearch {
	GtkHBox parent;
	MailShell *shell;
	MailView  *view;
	MailSearchPrivate *priv;
} MailSearch;

typedef struct _MailSearchClass {
	GtkHBoxClass parent_class;

	void (* search_activate)    (MailSearch *class);
	void (* search_clear)    (MailSearch *class);	
} MailSearchClass;

MailSearch * mail_search_new (void);
void mail_search_set_state (MailSearch *, const char *, const char *);

#endif



