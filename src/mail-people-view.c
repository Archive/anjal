/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <glib/gi18n.h>
#include "mail-people-view.h"
#include "mail-component.h"
#include <libedataserver/e-account-list.h>
#include "mail-view.h"
#include "mail/mail-config.h"

#include <anerley/anerley-ebook-feed.h>
#include <anerley/anerley-item.h>
#include <anerley/anerley-simple-grid-view.h>
#include <anerley/anerley-tile-view.h>
#include <anerley/anerley-tile.h>
#include <anerley/anerley-feed-model.h>
#include <anerley/anerley-econtact-item.h>

#include <clutter/clutter.h>
#include <clutter-gtk/clutter-gtk.h>

#include <glib.h>

/* HACK: Anerley should expose this formally */
void anerley_econtact_item_activate (AnerleyItem *item);

struct _MailPeopleViewPrivate {
	GtkWidget *tab_str;
  	EBook *book;
	ClutterActor *table;
};

G_DEFINE_TYPE (MailPeopleView, mail_people_view, GTK_TYPE_VBOX)

enum {
	VIEW_CLOSE,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
mail_people_view_init (MailPeopleView  *shell)
{
	shell->priv = g_new0(MailPeopleViewPrivate, 1);
}

static void
mail_people_view_finalize (GObject *object)
{
	/* MailPeopleView *shell = (MailPeopleView *)object; */

	G_OBJECT_CLASS (mail_people_view_parent_class)->finalize (object);
}

static void
mail_people_view_class_init (MailPeopleViewClass *klass)
{
	GObjectClass * object_class = G_OBJECT_CLASS (klass);

	mail_people_view_parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = mail_people_view_finalize;

	signals[VIEW_CLOSE] =
		g_signal_new ("view-close",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (MailPeopleViewClass , view_close),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

}

#define PACK_BOX(w,s) box = gtk_hbox_new(FALSE, 0); gtk_box_pack_start((GtkBox *)box, w, FALSE, FALSE, s); gtk_widget_show(box); gtk_widget_show(w); gtk_box_pack_start((GtkBox *)acview, box, FALSE, FALSE, 3);
static void
_entry_text_changed_cb (ClutterText  *text,
                        ClutterModel *model)
{
  const gchar *str;
  str = clutter_text_get_text (text);
  g_debug ("foo: %s", str);

  anerley_feed_model_set_filter_text ((AnerleyFeedModel *)model, str);
}

static void
mpv_size_alloc (GtkWidget *widget, GtkAllocation *allocation, MailPeopleView *mpv)
{
	clutter_actor_set_size (mpv->priv->table, allocation->width-10, allocation->height-6);
}

static void
mpv_item_activated (AnerleyTileView *view, AnerleyItem *item, MailPeopleView *mpv)
{
	anerley_econtact_item_activate (item);
}

void
mail_people_view_construct (MailPeopleView *acview)
{
	GError *error = NULL;
  	AnerleyEBookFeed *feed;
  	ClutterActor *stage;
  	NbtkWidget *scroll_view;
  	NbtkWidget *icon_view;
  	ClutterModel *model;
  	NbtkWidget *table, *entry;
  	ClutterActor *tmp;
	GtkWidget *embed; 

	acview->priv->book = e_book_new_default_addressbook (&error);
  	if (error) {
    		g_warning (G_STRLOC ": Error getting default addressbook: %s",
               		error->message);
    		g_clear_error (&error);
    		return;
  	}

  	feed = (AnerleyEBookFeed *) anerley_ebook_feed_new (acview->priv->book);
  	model = anerley_feed_model_new ((AnerleyFeed *)feed);
  	icon_view = anerley_tile_view_new ((AnerleyFeedModel *)model);

	embed = gtk_clutter_embed_new ();
	gtk_widget_show (embed);
	stage = gtk_clutter_embed_get_stage ((GtkClutterEmbed *)embed);

  	table = nbtk_table_new ();
  	entry = nbtk_entry_new (NULL);
  	tmp = nbtk_entry_get_clutter_text ((NbtkEntry *)entry);
  	g_signal_connect (tmp,
        	            "text-changed",
                	    G_CALLBACK(_entry_text_changed_cb),
                    	    model);
	g_signal_connect (icon_view, "item-activated", G_CALLBACK(mpv_item_activated), acview);
	nbtk_table_add_actor_with_properties (NBTK_TABLE (table),
                                        	(ClutterActor *)entry,
                                        	0,
                                        	0,
                                        	"x-fill",
                                        	TRUE,
                                        	"x-expand",
                                        	TRUE,
                                        	"y-expand",
                                        	FALSE,
                                        	NULL);

  	scroll_view = nbtk_scroll_view_new ();
  	clutter_container_add_actor (CLUTTER_CONTAINER (stage),
                               	(ClutterActor *)table);
  	clutter_container_add_actor (CLUTTER_CONTAINER (scroll_view),
                               	(ClutterActor *)icon_view);
  	nbtk_table_add_actor_with_properties (NBTK_TABLE (table),
                                        	(ClutterActor *)scroll_view,
                                        	1,
                                        	0,
                                        	"x-fill",
                                       		TRUE,
                                        	"x-expand",
                                        	TRUE,
                                        	"y-expand",
                                        	TRUE,
                                        	"y-fill",
                                        	TRUE,
                                        	NULL);

  	//clutter_actor_set_size (table, 640, 480);
	gtk_box_pack_start ((GtkBox *)acview, embed, TRUE, TRUE, 6);
	acview->priv->table = (ClutterActor *)table;
	g_signal_connect (acview, "size-allocate", G_CALLBACK(mpv_size_alloc), acview);
}

MailPeopleView *
mail_people_view_new ()
{
	MailPeopleView *view = g_object_new (MAIL_PEOPLE_VIEW_TYPE, NULL);
	view->type = MAIL_VIEW_PEOPLE;
	view->uri = "people://";

	mail_people_view_construct (view);
	
	return view;
}

static gboolean
msv_btn_expose (GtkWidget *w, GdkEventExpose *event, MailPeopleView *mfv)
{
	GdkPixbuf *img = g_object_get_data ((GObject *)w, "pbuf");
	cairo_t *cr = gdk_cairo_create (w->window);
	cairo_save (cr);
	gdk_cairo_set_source_pixbuf (cr, img, event->area.x-5, event->area.y-4);
	cairo_paint(cr);
	cairo_restore(cr);
	cairo_destroy (cr);

	return TRUE;
}

static void
msv_close (GtkButton *w, MailPeopleView *mfv)
{
	g_signal_emit (mfv, signals[VIEW_CLOSE], 0);			
}



GtkWidget *
mail_people_view_get_tab_widget(MailPeopleView *mcv)
{
	GdkPixbuf *pbuf = gtk_widget_render_icon ((GtkWidget *)mcv, "gtk-close", GTK_ICON_SIZE_MENU, NULL);

	GtkWidget *tool, *box, *img;
	int w=-1, h=-1;
	GtkWidget *tab_label;

	img = gtk_image_new_from_pixbuf (pbuf);
	g_object_set_data ((GObject *)img, "pbuf", pbuf);
	g_signal_connect (img, "expose-event", G_CALLBACK(msv_btn_expose), mcv);
	
	tool = gtk_button_new ();
	gtk_button_set_relief((GtkButton *)tool, GTK_RELIEF_NONE);
	gtk_button_set_focus_on_click ((GtkButton *)tool, FALSE);
	gtk_widget_set_tooltip_text (tool, _("Close Tab"));
	g_signal_connect (tool, "clicked", G_CALLBACK(msv_close), mcv);
	
	box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start ((GtkBox *)box, img, FALSE, FALSE, 0);
	gtk_container_add ((GtkContainer *)tool, box);
	gtk_widget_show_all (tool);
	gtk_icon_size_lookup_for_settings (gtk_widget_get_settings(tool) , GTK_ICON_SIZE_MENU, &w, &h);
	gtk_widget_set_size_request (tool, w+2, h+2);

	box = gtk_label_new (_("People"));
	tab_label = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start ((GtkBox *)tab_label, box, FALSE, FALSE, 0);
	gtk_box_pack_start ((GtkBox *)tab_label, tool, FALSE, FALSE, 0);
	gtk_widget_show_all (tab_label);

	return tab_label;
	
}

void
mail_people_view_activate (MailPeopleView *mcv, GtkWidget *tree, GtkWidget *folder_tree, GtkWidget *check_mail, GtkWidget *sort_by, GtkWidget *slider, gboolean act)
{
	 if (!folder_tree || !check_mail || !sort_by)
		  return;
	 if (!GTK_WIDGET_VISIBLE(folder_tree))
		 gtk_widget_show (slider);
	 gtk_widget_set_sensitive (check_mail, act);
	 gtk_widget_set_sensitive (sort_by, act);
}
