/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Johnny Jacob <jjohnny@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_COMPOSER_VIEW_H_
#define _MAIL_COMPOSER_VIEW_H_

#include <gtk/gtk.h>
#include <camel/camel.h>
#include "mail-view.h"

#define MAIL_COMPOSER_VIEW_TYPE        (mail_composer_view_get_type ())
#define MAIL_COMPOSER_VIEW(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), MAIL_COMPOSER_VIEW_TYPE, MailFolderView))
#define MAIL_COMPOSER_VIEW_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), MAIL_COMPOSER_VIEW_TYPE, MailFolderViewClass))
#define IS_MAIL_COMPOSER_VIEW(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAIL_COMPOSER_VIEW_TYPE))
#define IS_MAIL_COMPOSER_VIEW_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), MAIL_COMPOSER_VIEW_TYPE))
#define MAIL_COMPOSER_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), MAIL_COMPOSER_VIEW_TYPE, MailFolderViewClass))

typedef struct _MailComposerViewPrivate MailComposerViewPrivate;

typedef struct _MailComposerView {
	GtkVBox parent;
	int type;
	char *uri;
	MailViewFlags flags;

	 /* Base class ends */
	GtkWidget *tab_label;

	MailComposerViewPrivate *priv;
} MailComposerView;

typedef struct _MailComposerViewClass {
	GtkVBoxClass parent_class;

	/* Signals */
	 void (*message_shown) (MailComposerView *);	 
	void (* view_close) (MailComposerView *);
} MailComposerViewClass;

MailComposerView * mail_composer_view_new (void);
void mail_composer_view_activate (MailComposerView *mfv, GtkWidget *folder_tree, GtkWidget *check_mail, GtkWidget *sort_by, gboolean act);
void mail_composer_view_replace_composer (MailComposerView *mcv, GtkWidget *composer);
MailComposerView * mail_composer_view_new_with_composer (GtkWidget *composer);
gboolean mail_composer_view_can_quit (MailComposerView *mcv);

#endif
