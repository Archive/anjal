/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef EM_WEBKIT_STREAM_H
#define EM_WEBKIT_STREAM_H

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define EM_WEBKIT_STREAM_TYPE     (em_webkit_stream_get_type ())
#define EM_WEBKIT_STREAM(obj)     (CAMEL_CHECK_CAST((obj), EM_WEBKIT_STREAM_TYPE, EMWebKitStream))
#define EM_WEBKIT_STREAM_CLASS(k) (CAMEL_CHECK_CLASS_CAST ((k), EM_WEBKIT_STREAM_TYPE, EMWebKitStreamClass))
#define EM_IS_WEBKIT_STREAM(o)    (CAMEL_CHECK_TYPE((o), EM_WEBKIT_STREAM_TYPE))

#include "mail/em-sync-stream.h"
#include <webkit/webkit.h>

typedef struct _EMWebKitStream {
	EMSyncStream sync;

	WebKitWebView *view;
	GtkWidget *body;
	guint destroy_id;
	GString *buffer;
	int flags;
} EMWebKitStream;

typedef struct {
	EMSyncStreamClass parent_class;

} EMWebKitStreamClass;


CamelType    em_webkit_stream_get_type (void);

CamelStream *em_webkit_stream_new(WebKitWebView *, GtkWidget *);
void em_webkit_stream_set_flags (EMWebKitStream *emhs, int flags);
GtkWidget * em_webkit_stream_get_body (EMWebKitStream *emws);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* EM_WEBKIT_STREAM_H */
