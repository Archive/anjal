/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Michael Zucchi <notzed@ximian.com>
 *		Jeffrey Stedfast <fejj@ximian.com>
 *		Ettore Perazzoli <ettore@ximian.com>
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_COMPONENT_H_
#define _MAIL_COMPONENT_H_

#include <glib.h>
#include <gtk/gtk.h>

struct _CamelStore;

#define MAIL_TYPE_COMPONENT			(mail_component_get_type ())
#define MAIL_COMPONENT(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), MAIL_TYPE_COMPONENT, MailComponent))
#define MAIL_COMPONENT_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), MAIL_TYPE_COMPONENT, MailComponentClass))
#define MAIL_IS_COMPONENT(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAIL_TYPE_COMPONENT))
#define MAIL_IS_COMPONENT_CLASS(klass)		(G_TYPE_CHECK_CLASS_TYPE ((obj), MAIL_TYPE_COMPONENT))

typedef struct _MailComponent        MailComponent;
typedef struct _MailComponentPrivate MailComponentPrivate;
typedef struct _MailComponentClass   MailComponentClass;

enum _mail_component_folder_t {
	MAIL_COMPONENT_FOLDER_INBOX = 0,
	MAIL_COMPONENT_FOLDER_DRAFTS,
	MAIL_COMPONENT_FOLDER_OUTBOX,
	MAIL_COMPONENT_FOLDER_SENT,
	MAIL_COMPONENT_FOLDER_TEMPLATES,
	MAIL_COMPONENT_FOLDER_LOCAL_INBOX,
};

struct _MailComponent {
	 GObject  parent;

	 GtkWidget *folder_tree;
	 GtkWidget *people_view;
	 GtkWidget *mail_view;
	 GtkWidget *status_bar;

	 MailComponentPrivate *priv;
};

struct _MailComponentClass {
	GObjectClass parent_class;
};

typedef enum {
  USER_OFFLINE,
  FORCED_OFFLINE,
  USER_ONLINE
} MailShellState;

GType  mail_component_get_type  (void);

MailComponent *mail_component_peek  (void);

/* NOTE: Using NULL as the component implies using the default component */
const char       *mail_component_peek_base_directory    (MailComponent *component);
struct _RuleContext      *mail_component_peek_search_context    (MailComponent *component);
struct _EActivityHandler *mail_component_peek_activity_handler  (MailComponent *component);

struct _CamelSession *mail_component_peek_session(MailComponent *);

void        mail_component_add_store            (MailComponent *component,
						 struct _CamelStore    *store,
						 const char    *name);
struct _CamelStore *mail_component_load_store_by_uri    (MailComponent *component,
						 const char    *uri,
						 const char    *name);

void        mail_component_remove_store         (MailComponent *component,
						 struct _CamelStore    *store);
void        mail_component_remove_store_by_uri  (MailComponent *component,
						 const char    *uri);

int          mail_component_get_store_count  (MailComponent *component);
void         mail_component_stores_foreach   (MailComponent *component,
					      GHFunc         func,
					      void          *data);

void mail_component_remove_folder (MailComponent *component, struct _CamelStore *store, const char *path);

struct _EMFolderTreeModel *mail_component_peek_tree_model (MailComponent *component);

struct _CamelStore *mail_component_peek_local_store (MailComponent *mc);
struct _CamelFolder *mail_component_get_folder(MailComponent *mc, enum _mail_component_folder_t id);
const char *mail_component_get_folder_uri(MailComponent *mc, enum _mail_component_folder_t id);


void mail_indicate_new_mail (gboolean have_new_mail);
void mail_component_show_logger (gpointer);
MailComponent * mail_component_create (GtkWidget * parent, gboolean select_item);
gboolean mail_component_can_quit(MailComponent *mc);
gboolean mail_component_quit(MailComponent *mc);
void mail_component_show_status_bar (gboolean show);
#endif /* _MAIL_COMPONENT_H_ */
