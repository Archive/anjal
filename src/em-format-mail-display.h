/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 */

/*
  Concrete class for formatting mails to displayed html
*/

#ifndef EM_FORMAT_MAIL_DISPLAY_H
#define EM_FORMAT_MAIL_DISPLAY_H

#include "em-format-mail.h"

/* Standard GObject macros */
#define EM_TYPE_FORMAT_MAIL_DISPLAY \
	(em_format_mail_display_get_type ())
#define EM_FORMAT_MAIL_DISPLAY(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), EM_TYPE_FORMAT_MAIL_DISPLAY, EMFormatMailDisplay))
#define EM_FORMAT_MAIL_DISPLAY_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), EM_TYPE_FORMAT_MAIL_DISPLAY, EMFormatMailDisplayClass))
#define EM_IS_FORMAT_MAIL_DISPLAY(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), EM_TYPE_FORMAT_MAIL_DISPLAY))
#define EM_IS_FORMAT_MAIL_DISPLAY_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), EM_TYPE_FORMAT_MAIL_DISPLAY))
#define EM_FORMAT_MAIL_DISPLAY_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), EM_TYPE_FORMAT_MAIL_DISPLAY, EMFormatMailDisplayClass))

G_BEGIN_DECLS

typedef struct _EMFormatMailDisplay EMFormatMailDisplay;
typedef struct _EMFormatMailDisplayClass EMFormatMailDisplayClass;
typedef struct _EMFormatMailDisplayPrivate EMFormatMailDisplayPrivate;

struct _EMFormatMailDisplay {
	EMFormatMail formathtml;

	EMFormatMailDisplayPrivate *priv;

	struct _ESearchingTokenizer *search_tok;

	guint animate:1;
	guint caret_mode:1;
	guint nobar:1;
};

#define EM_FORMAT_MAIL_DISPLAY_SEARCH_PRIMARY (0)
#define EM_FORMAT_MAIL_DISPLAY_SEARCH_SECONDARY (1)
#define EM_FORMAT_MAIL_DISPLAY_SEARCH_ICASE (1<<8)

struct _EMFormatMailDisplayClass {
	EMFormatMailClass formathtml_class;

	/* a link clicked normally */
	void (*link_clicked)(EMFormatMailDisplay *efhd, const gchar *uri);
	/* a part or a link button pressed event */
	gint (*popup_event)(EMFormatMailDisplay *efhd, GdkEventButton *event, const gchar *uri, struct _CamelMimePart *part);
	/* the mouse is over a link */
	void (*on_url)(EMFormatMailDisplay *efhd, const gchar *uri);
};

GType		em_format_mail_display_get_type	(void);
EMFormatMailDisplay *
		em_format_mail_display_new	(void);

void		em_format_mail_display_goto_anchor
						(EMFormatMailDisplay *efhd,
						 const gchar *name);

void		em_format_mail_display_set_animate
						(EMFormatMailDisplay *efhd,
						 gboolean state);
void		em_format_mail_display_set_caret_mode
						(EMFormatMailDisplay *efhd,
						 gboolean state);

void		em_format_mail_display_set_search
						(EMFormatMailDisplay *efhd,
						 gint type,
						 GSList *strings);
void		em_format_mail_display_search	(EMFormatMailDisplay *efhd);
void		em_format_mail_display_search_with
						(EMFormatMailDisplay *efhd,
						 gchar *word);
void		em_format_mail_display_search_close
						(EMFormatMailDisplay *efhd);

void		em_format_mail_display_cut	(EMFormatMailDisplay *efhd);
void		em_format_mail_display_copy	(EMFormatMailDisplay *efhd);
void		em_format_mail_display_paste	(EMFormatMailDisplay *efhd);

void		em_format_mail_display_zoom_in	(EMFormatMailDisplay *efhd);
void		em_format_mail_display_zoom_out	(EMFormatMailDisplay *efhd);
void		em_format_mail_display_zoom_reset
						(EMFormatMailDisplay *efhd);

gboolean	em_format_mail_display_popup_menu
						(EMFormatMailDisplay *efhd);
void		em_format_mail_display_set_attachment_pane 
						(EMFormatMailDisplay *efmd, 
						 GtkWidget *box);

G_END_DECLS

#endif /* EM_FORMAT_MAIL_DISPLAY_H */
