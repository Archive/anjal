/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef CUSTOM_CELL_RENDERER_TOGGLE_PIXBUF_H
#define CUSTOM_CELL_RENDERER_TOGGLE_PIXBUF_H

#include <gdk/gdk.h>
#include <gtk/gtkcellrenderer.h>


G_BEGIN_DECLS


#define CUSTOM_TYPE_CELL_RENDERER_TOGGLE_PIXBUF			(custom_cell_renderer_toggle_pixbuf_get_type ())
#define CUSTOM_CELL_RENDERER_TOGGLE_PIXBUF(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), CUSTOM_TYPE_CELL_RENDERER_TOGGLE_PIXBUF, CustomCellRendererTogglePixbuf))
#define CUSTOM_CELL_RENDERER_TOGGLE_PIXBUF_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), CUSTOM_TYPE_CELL_RENDERER_TOGGLE_PIXBUF, CustomCellRendererTogglePixbufClass))
#define CUSTOM_IS_CELL_RENDERER_TOGGLE_PIXBUF(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), CUSTOM_TYPE_CELL_RENDERER_TOGGLE_PIXBUF))
#define CUSTOM_IS_CELL_RENDERER_TOGGLE_PIXBUF_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), CUSTOM_TYPE_CELL_RENDERER_TOGGLE_PIXBUF))
#define CUSTOM_CELL_RENDERER_TOGGLE_PIXBUF_GET_CLASS(obj)         (G_TYPE_INSTANCE_GET_CLASS ((obj), CUSTOM_TYPE_CELL_RENDERER_TOGGLE_PIXBUF, CustomCellRendererTogglePixbufClass))

typedef struct _CustomCellRendererTogglePixbuf CustomCellRendererTogglePixbuf;
typedef struct _CustomCellRendererTogglePixbufClass CustomCellRendererTogglePixbufClass;

struct _CustomCellRendererTogglePixbuf
{
  GtkCellRenderer parent;

  guint active : 1;
};

struct _CustomCellRendererTogglePixbufClass
{
  GtkCellRendererClass parent_class;

  void (* toggled) (CustomCellRendererTogglePixbuf *cell_renderer_toggle,
		    const gchar                 *path);

};
GType                custom_cell_renderer_toggle_pixbuf_get_type (void);

GtkCellRenderer *custom_cell_renderer_toggle_pixbuf_new       (GdkPixbuf *active, GdkPixbuf *inactive);


G_END_DECLS

#endif /* CUSTOM_CELL_RENDERER_TOGGLE_PIXBUF_H */
