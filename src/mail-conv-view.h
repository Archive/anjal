/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_CONVERSATION_VIEW_H_
#define _MAIL_CONVERSATION_VIEW_H_

#include <gtk/gtk.h>
#include <camel/camel.h>
#include <mail-message-view.h>
#include "mail-view.h"

#define MAIL_CONV_VIEW_TYPE        (mail_conv_view_get_type ())
#define MAIL_CONV_VIEW(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), MAIL_FOLDER_VIEW_TYPE, MailConvView))
#define MAIL_CONV_VIEW_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), MAIL_CONV_VIEW_TYPE, MailConvViewClass))
#define IS_MAIL_CONV_VIEW(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAIL_CONV_VIEW_TYPE))
#define IS_MAIL_CONV_VIEW_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), MAIL_CONV_VIEW_TYPE))
#define MAIL_CONV_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), MAIL_CONV_VIEW_TYPE, MailConvViewClass))

typedef struct _MailConvViewPrivate MailConvViewPrivate;


typedef struct _MailConvView {
	GtkVBox parent;
	int type;
	char *uri;
	MailViewFlags flags;
	 
	/* Base class */
	GtkWidget *header;
	GtkWidget *scroller;
	GtkWidget *child_box;
	
	GList *children;
	MailConvViewPrivate *priv;
} MailConvView;

typedef struct _MailConvViewClass {
	GtkVBoxClass parent_class;

	 void (*message_shown) (MailConvView *);
	void (* view_close) (MailConvView *);	
} MailConvViewClass;

MailConvView * mail_conv_view_new (void);
void mail_conv_view_set_thread (MailConvView *, CamelFolder *folder, const char *uri, GPtrArray *);
void mail_conv_view_activate (MailConvView *mcv, GtkWidget *tree, GtkWidget *folder_tree, GtkWidget *check_mail, GtkWidget *sort_by, gboolean act);
GtkWidget *mail_conv_view_get_tab_widget (MailConvView *);
#endif
