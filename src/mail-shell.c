/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <gconf/gconf-client.h>
#include "mail-shell.h"
#include "mail-view.h"
#include <gdk/gdkkeysyms.h>
#include <misc/e-spinner.h>
#include "mail-decoration.h"
#include <mail/em-utils.h>
#include <mail/em-composer-utils.h>
#include <e-util/e-util.h>
#include <shell/e-shell.h>
#include <shell/e-shell-window-actions.h>
#include <shell/e-shell-view.h>
#include <shell/e-shell-taskbar.h>
#include <shell/e-shell-searchbar.h>

enum {
	BACKSPACE_PRESSED,
	SLASH_PRESSED,
	CTRL_W_PRESSED,
	CTRL_Q_PRESSED,
	F5_PRESSED,

	LAST_SIGNAL
};

/* Re usable colors */

GdkColor *pcolor_sel;
char *scolor_sel;
GdkColor *pcolor_fg_sel;
char *scolor_fg_sel;
GdkColor *pcolor_bg_norm;
char *scolor_bg_norm;
GdkColor *pcolor_norm;
char *scolor_norm;
GdkColor *pcolor_fg_norm;
char *scolor_fg_norm;

static MailShell * default_shell;

static guint mail_shell_signals[LAST_SIGNAL];

/* Hacks :-( */
void anjal_shell_view_set_mail_view (EShellView *view, MailView *mview);
GtkWidget * anjal_shell_content_get_search_entry (EShellContent *);
void anjal_shell_content_pack_view (EShellContent *, MailView *);
void e_shell_window_private_constructed (EShellWindow *);
void e_shell_window_update_title (EShellWindow *shell_window);

static gboolean ms_check_new (void);

extern gboolean windowed;

struct  _MailShellPrivate {

	GtkWidget *box;
	
	GtkWidget * top_bar;
	GtkWidget *side_pane;
	GtkWidget *message_pane;
	GtkWidget *bottom_bar;
	GtkWidget *folder_pane;

	MailView *view;

	/* Top Bar */
	GtkWidget *action_bar;
	GtkWidget *search_bar;
	GtkWidget *option_bar;

	GtkWidget *check_mail;
	GtkWidget *new_mail;

	GtkWidget *search_entry;
	GtkWidget *search_box;

	GtkWidget *sort;
	GtkWidget *settings;
	GtkWidget *quit;	
	GtkWidget *slider;
	
	MailViewChild *people;
	MailViewChild *settings_view;

	EShellTaskbar *task_bar;
	
	guint task_bar_id;

	char **args;
};

static void mail_shell_quit (MailShell *shell);
static void new_email_btn_clicked (GtkButton *button G_GNUC_UNUSED, gpointer data);

G_DEFINE_TYPE (MailShell, mail_shell, E_TYPE_SHELL_WINDOW)
#define PACK_IN_TOOL(wid,icon)	{ GtkWidget *tbox; tbox = gtk_hbox_new (FALSE, 0); gtk_box_pack_start ((GtkBox *)tbox, gtk_image_new_from_icon_name(icon, GTK_ICON_SIZE_BUTTON), FALSE, FALSE, 0); wid = (GtkWidget *)gtk_tool_button_new (tbox, NULL); }


static void setup_abooks (void);
static int color_expose (GtkWidget *w, GdkEventExpose *event G_GNUC_UNUSED, gpointer data);
static void ms_check_mail  (GtkButton *button G_GNUC_UNUSED, gpointer data);
static void popup_sort (GtkButton *b, MailShell *shell);
static void settings_btn_clicked (GtkButton *button G_GNUC_UNUSED, gpointer data);
static void mail_shell_quit_cb (GtkWidget *w G_GNUC_UNUSED, MailShell *shell);
static void ms_delete_event (MailShell *shell, GdkEvent *event G_GNUC_UNUSED, gpointer data G_GNUC_UNUSED);
static void ms_init_style (GtkStyle *style);

static void
ms_remove_settings  (MailViewChild *mvc G_GNUC_UNUSED,
		     MailShell *shell)
{
	shell->priv->settings_view = NULL;
}

#if 0
static void
ms_construct_content_area (EShellWindow *shell)
{

}
#endif

static gboolean
cleanup_status (EShellWindow *eshell)
{
	MailShell *shell = (MailShell *)eshell;
	int count = e_shell_taskbar_get_activity_count (shell->priv->task_bar);

	if (count == 0 && e_shell_window_get_taskbar_visible (eshell))
		e_shell_window_set_taskbar_visible (eshell, FALSE);


	return TRUE;
}

static void
ms_activity_added (EShellWindow *window, EActivity *activity)
{
	e_shell_window_set_taskbar_visible (window, TRUE);
}

static void
handle_cmdline (MailView *mv, MailShell *shell)
{
	g_signal_handlers_block_by_func (mv, handle_cmdline, shell);
	mail_shell_handle_cmdline (shell);
}

static void
ms_show_post_druid (MailViewChild *mfv G_GNUC_UNUSED,
		    MailShell *shell)
{
	gtk_widget_show (shell->priv->view->folder_tree);
	if (shell->priv->settings_view)
		mail_view_switch_to_settings ((MailView *)shell->priv->view, (MailViewChild *)shell->priv->settings_view);
	else {
		shell->priv->settings_view = mail_view_add_page ((MailView *)shell->priv->view, MAIL_VIEW_SETTINGS, NULL);
		g_signal_connect (shell->priv->settings_view, "view-close", G_CALLBACK(ms_remove_settings), shell);
	}

}

static gboolean
idle_cb (EShellWindow *eshell)
{
	GConfClient *client = gconf_client_get_default();
	const gchar *key;
	GError *error = NULL;
	EShellView *shell_view;
	MailShell *shell = (MailShell *)eshell;
	MailShellPrivate *priv = shell->priv;
	MailView *view = mail_view_new ();
	EShell *es;
	EShellBackend *backend;

	shell->priv->view = view;
	//client = e_shell_get_gconf_client (shell);
	key = "/apps/evolution/shell/view_defaults/component_id";
	gconf_client_set_string (client, key, "anjal", &error);
	e_shell_window_set_active_view (eshell, "anjal");
	//e_shell_window_switch_to_view (eshell, "mail");
	g_object_unref (client);
	g_signal_connect ((GObject *)view, "view-new", G_CALLBACK(handle_cmdline), shell);

	shell_view = e_shell_window_get_shell_view (eshell, "anjal");
	mail_view_set_shell_view (view, shell_view);
	anjal_shell_view_set_mail_view (shell_view, view);

	g_signal_handlers_disconnect_by_func (shell_view, e_shell_window_update_title, shell);
	gtk_window_set_title (shell, _("Mail"));

	//((MailSearch *)priv->search_entry)->shell = shell;
	//gtk_widget_show (priv->search_entry);
	priv->search_entry = anjal_shell_content_get_search_entry (e_shell_view_get_shell_content(shell_view));
	mail_view_set_search_entry (view, priv->search_entry);
	mail_view_set_sort_by (view, priv->sort);
	mail_view_set_check_email (view, priv->check_mail);

	gtk_widget_unparent (priv->search_entry);
	gtk_box_pack_start ((GtkBox *)priv->search_box, priv->search_entry, TRUE, TRUE, 0);
	gtk_widget_show_all (priv->search_box);
	e_shell_searchbar_set_filter_visible ((EShellSearchbar *) priv->search_entry, FALSE);
	e_shell_searchbar_set_scope_visible ((EShellSearchbar *)priv->search_entry, FALSE);
	e_shell_searchbar_set_label_visible ((EShellSearchbar *)priv->search_entry, FALSE);

//	anjal_shell_content_set_view (e_shell_view_get_shell_content(shell_view), view);
//	gtk_container_add (e_shell_view_get_shell_content(shell_view), view);
	anjal_shell_content_pack_view (e_shell_view_get_shell_content(shell_view), view);
	gtk_widget_show ((GtkWidget *)view);
	
	shell->priv->task_bar = e_shell_view_get_shell_taskbar (shell_view);
	shell->priv->task_bar_id = g_timeout_add (10 * 1000, (GSourceFunc) cleanup_status, eshell);
	e_shell_window_set_taskbar_visible (eshell, FALSE);
	es = e_shell_get_default ();
	backend = e_shell_get_backend_by_name (es, "anjal");
	g_signal_connect_swapped (backend, "activity-added", G_CALLBACK(ms_activity_added), eshell);

	if (ms_check_new()) {
		MailViewChild *mc;
		char *pdir = g_build_filename (g_get_home_dir(), ".gnome2_private", NULL);

		//gtk_widget_hide (((MailView *) shell->mail_component->mail_view)->folder_tree);
		mc = mail_view_add_page ((MailView *)view, MAIL_VIEW_ACCOUNT, NULL);
		g_signal_connect (mc, "view-close", G_CALLBACK(ms_show_post_druid), shell);
		setup_abooks ();
		if (!g_file_test(pdir, G_FILE_TEST_EXISTS)) {
			g_mkdir (pdir, 0700);
		}
		g_free (pdir);
	}	
	return FALSE;
}

static GtkWidget *
ms_construct_toolbar (EShellWindow *eshell)
{
	MailShell *shell = (MailShell *)eshell;
	MailShellPrivate *priv = shell->priv;
	GtkWidget *box, *tmp, *lbl, *ar1;
	
	priv->top_bar = gtk_toolbar_new ();
	gtk_widget_set_name (priv->top_bar, "MoblinToolbar");
	gtk_window_set_title ((GtkWindow *)eshell, _("Mail"));

	gtk_widget_show_all (priv->top_bar);
	if (g_getenv("ANJAL_NO_MAX") || windowed) {
		gtk_container_set_border_width (GTK_CONTAINER (shell), 1);
		g_signal_connect (priv->top_bar, "expose-event",
						  G_CALLBACK (color_expose),
						  shell);
		/* Leave it to the theme to decide the height */
		/* gtk_widget_set_size_request (priv->top_bar, -1, 42);	*/
	}

	/* Check mail button and the spinner */
	priv->check_mail = (GtkWidget *)gtk_tool_button_new (NULL, NULL);
	gtk_widget_set_tooltip_text (priv->check_mail, _("Check Mail"));
	box = gtk_hbox_new (FALSE, 0);
	tmp = e_spinner_new_spinning_small_shown();
	gtk_widget_hide(tmp);
	g_object_set_data ((GObject *)priv->check_mail, "spinner", tmp);
	gtk_box_pack_start ((GtkBox *)box, tmp, FALSE, FALSE, 0);
	tmp = gtk_image_new_from_icon_name("mail-send-receive", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show(tmp);
	g_object_set_data ((GObject *)priv->check_mail, "icon", tmp);
	gtk_box_pack_start ((GtkBox *)box, tmp, FALSE, FALSE, 0);
	gtk_widget_show(box);
	gtk_tool_button_set_icon_widget ((GtkToolButton *)priv->check_mail, box);
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->check_mail, 0);
	gtk_widget_show (priv->check_mail);
	g_signal_connect (priv->check_mail, "clicked", G_CALLBACK(ms_check_mail), shell);

	/* New mail tool button */
	PACK_IN_TOOL(priv->new_mail, "mail-message-new");
	gtk_widget_set_tooltip_text (priv->new_mail, _("New Mail"));
	gtk_widget_show_all (priv->new_mail);
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->new_mail, 1);
	g_signal_connect (priv->new_mail, "clicked", G_CALLBACK (new_email_btn_clicked), shell);

	/* Separator before Search */
	tmp = (GtkWidget *)gtk_separator_tool_item_new ();
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)tmp, 2);
	gtk_separator_tool_item_set_draw  ((GtkSeparatorToolItem *)tmp, FALSE);
	gtk_widget_set_size_request (tmp, 20, -1);
	gtk_widget_show (tmp);

	/* Search bar */
	priv->search_bar = (GtkWidget *)gtk_tool_item_new ();
	gtk_tool_item_set_expand ((GtkToolItem *)priv->search_bar, TRUE);
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->search_bar, 3);
	gtk_widget_show (priv->search_bar);
	tmp = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (tmp);
	priv->search_box = tmp;
	//priv->search_entry = (GtkWidget *)mail_search_new ();
	//((MailSearch *)priv->search_entry)->shell = shell;
	//gtk_widget_show (priv->search_entry);
	//gtk_box_pack_start ((GtkBox *)tmp, priv->search_entry, TRUE, FALSE, 0);
	gtk_container_add ((GtkContainer *)priv->search_bar, tmp);

	/* Post search separator */
	tmp = (GtkWidget *)gtk_separator_tool_item_new ();
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)tmp, 4);
	gtk_separator_tool_item_set_draw  ((GtkSeparatorToolItem *)tmp, FALSE);
	gtk_widget_set_size_request (tmp, 20, -1);
	gtk_widget_show (tmp);

	/* Sort combo */
	tmp = gtk_hbox_new (FALSE, 0);
	lbl =  gtk_label_new (_("Sort By"));
	ar1 = gtk_arrow_new(GTK_ARROW_DOWN, GTK_SHADOW_NONE);

#if 0	
	if (windowed ||  g_getenv("ANJAL_NO_MAX") != NULL) {
		GdkColor color;

		gdk_color_parse ("#000000", &color);
		gtk_widget_modify_fg (lbl, GTK_STATE_SELECTED, &color);
		gtk_widget_modify_fg (lbl, GTK_STATE_ACTIVE, &color);
		gtk_widget_modify_fg (lbl, GTK_STATE_PRELIGHT, &color);

		gdk_color_parse ("#ffffff", &color);
		gtk_widget_modify_fg (lbl, GTK_STATE_NORMAL, &color);
	}
#endif

	gtk_box_pack_start ((GtkBox *)tmp, lbl, FALSE, FALSE, 6);
	gtk_box_pack_start ((GtkBox *)tmp, ar1, FALSE, FALSE, 0);
	gtk_widget_show_all (tmp);
	priv->sort = (GtkWidget *)gtk_tool_button_new  (tmp, NULL);
	gtk_widget_show_all (priv->sort);
	g_signal_connect (priv->sort, "clicked", G_CALLBACK(popup_sort), shell);
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->sort, 5);

	/* Settings button */
	PACK_IN_TOOL(priv->settings, "preferences-system");
	gtk_widget_set_tooltip_text(priv->settings, _("Settings"));
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->settings, 6);
	gtk_widget_show_all (priv->settings);
	g_signal_connect (priv->settings, "clicked", G_CALLBACK (settings_btn_clicked), shell);

	/* Post Settings separator */
	tmp = (GtkWidget *)gtk_separator_tool_item_new ();
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)tmp, 7);
	gtk_separator_tool_item_set_draw  ((GtkSeparatorToolItem *)tmp, FALSE);
	gtk_widget_show (tmp);
	
	/* Close button */
	PACK_IN_TOOL(priv->quit, "gtk-close");
	gtk_widget_set_tooltip_text(priv->quit, _("Quit"));
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->quit, 8);
	gtk_widget_show_all (priv->quit);
	g_signal_connect (priv->quit, "clicked", G_CALLBACK(mail_shell_quit_cb), shell);
	
	g_idle_add ((GSourceFunc)idle_cb, shell);

	return priv->top_bar;
}


static void
ms_constructed (GObject *object)
{
	MailShell *shell = (MailShell *)object;
	GtkStyle *style = gtk_widget_get_default_style ();
	int window_width = 1024;

	e_shell_window_private_constructed (E_SHELL_WINDOW (object));
	gtk_window_set_icon_name ((GtkWindow *)shell, "evolution");
	ms_init_style (style);
	g_signal_connect ((GObject *)shell, "delete-event", G_CALLBACK (ms_delete_event), NULL);
	gtk_window_set_type_hint ((GtkWindow *)shell, GDK_WINDOW_TYPE_HINT_NORMAL);
	if (g_getenv("ANJAL_NO_MAX") == NULL && !windowed) {
		 GdkScreen *scr = gtk_widget_get_screen ((GtkWidget *)shell);
		 window_width = gdk_screen_get_width(scr);
		 gtk_window_set_default_size ((GtkWindow *)shell, gdk_screen_get_width(scr), gdk_screen_get_height (scr));
		 gtk_window_set_decorated ((GtkWindow *)shell, FALSE);
	} else  {
		mail_decoration_new ((GtkWindow *)shell);
	}
	gtk_window_set_title ((GtkWindow *)shell, _("Mail"));
	
}

static void
mail_shell_init (MailShell  *shell)
{
	GtkWidget *widget;
	GConfClient *client = gconf_client_get_default();

	shell->priv = g_new0 (MailShellPrivate, 1);
	shell->priv->people = NULL;
	shell->priv->settings_view = NULL;

	widget = e_shell_window_get_managed_widget (
		(EShellWindow *)shell, "/main-menu");
	gtk_widget_hide (widget);

	widget = e_shell_window_get_managed_widget (
		(EShellWindow *)shell, "/main-toolbar");
	gtk_widget_hide (widget);
	gconf_client_set_bool (client, "/apps/evolution/shell/view_defaults/buttons_visible", FALSE, NULL);
	g_object_unref (client);
	
}

static void
mail_shell_finalize (GObject *object)
{
	//FIXME: Unintialize the cleanup timeout.
	
	G_OBJECT_CLASS (mail_shell_parent_class)->finalize (object);
}
static void
ms_backspace_pressed (MailShell *shell)
{
	MailShellPrivate *priv = shell->priv;

	//gtk_widget_hide (priv->slider);
	//gtk_widget_show (shell->priv->folder_pane);
	mail_view_show_list ((MailView *)shell->priv->view);
}

static void
ms_ctrl_w_pressed (MailShell *shell)
{
	MailShellPrivate *priv = shell->priv;

	//gtk_widget_hide (priv->slider);
	//gtk_widget_show (shell->priv->folder_pane);
	mail_view_close_view ((MailView *)shell->priv->view);
}

static void
ms_slash_pressed (MailShell *shell)
{
	MailShellPrivate *priv = shell->priv;

	gtk_widget_grab_focus (priv->search_entry);
}

static void
ms_f5_pressed (MailShell *shell)
{
	MailView *view = (MailView *)shell->priv->view;

	mail_view_check_mail ((MailView *)view, FALSE);
}

static void
ms_ctrl_q_pressed (MailShell *shell)
{
	mail_shell_quit (shell);
}	
static void
mail_shell_class_init (MailShellClass *klass)
{
	GObjectClass * object_class = G_OBJECT_CLASS (klass);
	EShellWindowClass *shell_class;
	GtkBindingSet *binding_set;

	mail_shell_parent_class = g_type_class_peek_parent (klass);
	shell_class =  MAIL_SHELL_CLASS(klass);
	object_class->finalize = mail_shell_finalize;
	//shell_class->construct_content_area = ms_construct_content_area;
	shell_class->construct_toolbar = ms_construct_toolbar;
	shell_class->construct_menubar = NULL;
	object_class->constructed = ms_constructed;
	klass->backspace_pressed = ms_backspace_pressed;
	klass->ctrl_w_pressed = ms_ctrl_w_pressed;
	klass->slash_pressed = ms_slash_pressed;
	klass->f5_pressed = ms_f5_pressed;
	klass->ctrl_q_pressed = ms_ctrl_q_pressed;

	mail_shell_signals [BACKSPACE_PRESSED] =
		g_signal_new ("backspace_pressed",
				G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
				G_STRUCT_OFFSET (MailShellClass, backspace_pressed),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);

	mail_shell_signals [CTRL_W_PRESSED] =
		g_signal_new ("ctrl_w_pressed",
				G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
				G_STRUCT_OFFSET (MailShellClass, ctrl_w_pressed),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
	
	mail_shell_signals [SLASH_PRESSED] =
		g_signal_new ("slash_pressed",
				G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
				G_STRUCT_OFFSET (MailShellClass, slash_pressed),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);

	mail_shell_signals [CTRL_Q_PRESSED] =
		g_signal_new ("ctrl_q_pressed",
				G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
				G_STRUCT_OFFSET (MailShellClass, ctrl_q_pressed),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
	mail_shell_signals [F5_PRESSED] =
		g_signal_new ("f5_pressed",
				G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
				G_STRUCT_OFFSET (MailShellClass, f5_pressed),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);

	binding_set = gtk_binding_set_by_class (klass);
	gtk_binding_entry_add_signal (binding_set, GDK_W, GDK_CONTROL_MASK, "ctrl_w_pressed", 0);
	gtk_binding_entry_add_signal (binding_set, GDK_w, GDK_CONTROL_MASK, "ctrl_w_pressed", 0);
	gtk_binding_entry_add_signal (binding_set, GDK_BackSpace, 0, "backspace_pressed", 0);
	gtk_binding_entry_add_signal (binding_set, GDK_slash, 0, "slash_pressed", 0);
	gtk_binding_entry_add_signal (binding_set, GDK_Q, GDK_CONTROL_MASK, "ctrl_q_pressed", 0);
	gtk_binding_entry_add_signal (binding_set, GDK_Q, GDK_CONTROL_MASK, "ctrl_q_pressed", 0);
	gtk_binding_entry_add_signal (binding_set, GDK_F5, 0, "f5_pressed", 0);

};

static int
color_expose (GtkWidget *w,
	      GdkEventExpose *event G_GNUC_UNUSED,
	      gpointer data)
{
	GtkWindow *win = (GtkWindow *)data;
	cairo_t *cr = gdk_cairo_create (w->window);
	int wid = w->allocation.width;
	int heig = w->allocation.height;
	int wwid, wheig;
	GdkColor paint;

	gtk_window_get_size (win, &wwid, &wheig);
	gdk_color_parse ("#000000", &paint);
	gdk_cairo_set_source_color (cr,  &(paint));
	cairo_rectangle (cr, 0, 0, wwid, wheig);
	cairo_stroke (cr);

	gdk_color_parse ("#000000", &paint);
	gdk_cairo_set_source_color (cr,  &(paint));
	cairo_rectangle (cr, 1, 1, wid, heig);
	cairo_fill (cr);

	cairo_destroy (cr);

	return FALSE;
}

static void
popup_sort (GtkButton *b, MailShell *shell)
{
	mail_view_show_sort_popup ((MailView *)shell->priv->view, (GtkWidget *)b);
}

static void
new_email_btn_clicked (GtkButton *button G_GNUC_UNUSED,
		       gpointer data)
{
	MailShell *shell = (MailShell *) data;
	MailView *view = (MailView *)shell->priv->view;

	mail_view_add_page ((MailView *)view, MAIL_VIEW_COMPOSER, NULL);
}

MailViewChild *
mail_shell_create_composer (void)
{
	MailView *view = (MailView *)default_shell->priv->view;

	/* HACK: We need to distinguis this way of creating composer :/ */
	return mail_view_add_page ((MailView *)view, MAIL_VIEW_COMPOSER, (gpointer)-1);
}

MailViewChild *
mail_shell_create_composer_mailto (const char *uri)
{
	MailView *view = (MailView *)default_shell->priv->view;
	GtkWidget *composer = (GtkWidget *) em_utils_compose_new_message_with_mailto (uri, NULL);
	gtk_widget_show (view->folder_tree);

	return mail_view_add_page (view, MAIL_VIEW_COMPOSER, (gpointer)composer);
}
static void
ms_check_mail  (GtkButton *button G_GNUC_UNUSED,
		gpointer data)
{
	MailShell *shell = (MailShell *) data;
	MailView *view = (MailView *)shell->priv->view;

	mail_view_check_mail ((MailView *)view, TRUE);
}



static void
settings_btn_clicked (GtkButton *button G_GNUC_UNUSED,
		      gpointer data)
{
	MailShell *shell = (MailShell *) data;
	MailView *view = (MailView *)shell->priv->view;

	if (shell->priv->settings_view) {
		mail_view_switch_to_settings ((MailView *)view, (MailViewChild *)shell->priv->settings_view);
	} else {
		shell->priv->settings_view = mail_view_add_page ((MailView *)view, MAIL_VIEW_SETTINGS, NULL);
		g_signal_connect (shell->priv->settings_view, "view-close", G_CALLBACK(ms_remove_settings), shell);
	}
}

static void
ms_init_style (GtkStyle *style)
{
	 pcolor_sel = &style->base[GTK_STATE_SELECTED];
	 scolor_sel =  gdk_color_to_string (pcolor_sel);

	 pcolor_norm = &style->bg[GTK_STATE_NORMAL];
	 scolor_norm =  gdk_color_to_string (pcolor_norm);

	 pcolor_bg_norm = &style->base[GTK_STATE_NORMAL];
	 scolor_bg_norm = gdk_color_to_string (pcolor_bg_norm);

	 pcolor_fg_sel =&style->fg[GTK_STATE_SELECTED];
	 scolor_fg_sel = gdk_color_to_string (pcolor_fg_sel);

	 pcolor_fg_norm =&style->fg[GTK_STATE_NORMAL];
	 scolor_fg_norm = gdk_color_to_string (pcolor_fg_norm);
}

static void
mail_shell_quit (MailShell *mshell)
{
	EShell *shell;

	mail_view_save(mshell->priv->view);
	shell = e_shell_window_get_shell ((EShellWindow *)mshell);
	e_shell_quit (shell);
	
	/* What other clean ups ?*/
	//gboolean quit = 0;//mail_component_can_quit (shell->mail_component);
	//if (quit) {
	//	mail_view_save((MailView *)shell->mail_component->mail_view); 
	//	mail_component_quit (shell->mail_component);
	//	gtk_main_quit();
	//}
}

static void
mail_shell_quit_cb (GtkWidget *w G_GNUC_UNUSED,
		    MailShell *shell)
{
	mail_shell_quit (shell);
}

static void
ms_delete_event (MailShell *shell,
		 GdkEvent *event G_GNUC_UNUSED,
		 gpointer data G_GNUC_UNUSED)
{
	mail_shell_quit (shell);
}

static gboolean 
ms_check_new (void)
{
	GConfClient *client;
	GSList *accounts;

	client = gconf_client_get_default ();
	accounts = gconf_client_get_list (client, "/apps/evolution/mail/accounts", GCONF_VALUE_STRING, NULL);
	g_object_unref (client);

	if (accounts != NULL) {
		g_slist_foreach (accounts, (GFunc) g_free, NULL);
		g_slist_free (accounts);

		return FALSE;
	}
	
	return TRUE;
}

#if HAVE_ANERLEY
static void
ms_unlink_people (MailViewChild *mvc G_GNUC_UNUSED,
		  MailShell *shell)
{
	shell->priv->people = NULL;
}

static void
ms_show_people (GtkButton *button G_GNUC_UNUSED,
		MailShell *shell)
{
	if (!shell->priv->people) {
	//	shell->priv->people = mail_view_add_page ((MailView *)shell->mail_component->mail_view, MAIL_VIEW_PEOPLE, NULL);
		g_signal_connect(shell->priv->people, "view-close", G_CALLBACK(ms_unlink_people), shell);
	} else {
	//	mail_view_switch_to_people ((MailView *)shell->mail_component->mail_view, shell->priv->people);
	}
}
#endif




#if 0
void
mail_shell_construct (MailShell *shell)
{
	MailShellPrivate *priv = shell->priv;
	GtkWidget *tmp, *img, *box, *ar1, *ar2, *lbl;
	GtkStyle *style = gtk_widget_get_default_style ();
	int window_width = 1024;

	gtk_window_set_icon_name ((GtkWindow *)shell, "evolution");
	ms_init_style (style);
	g_signal_connect ((GObject *)shell, "delete-event", G_CALLBACK (ms_delete_event), NULL);
	gtk_window_set_type_hint ((GtkWindow *)shell, GDK_WINDOW_TYPE_HINT_NORMAL);
	if (g_getenv("ANJAL_NO_MAX") == NULL && !windowed) {
		 GdkScreen *scr = gtk_widget_get_screen ((GtkWidget *)shell);
		 window_width = gdk_screen_get_width(scr);
		 gtk_window_set_default_size ((GtkWindow *)shell, gdk_screen_get_width(scr), gdk_screen_get_height (scr));
		 gtk_window_set_decorated ((GtkWindow *)shell, FALSE);
	} else  {
		mail_decoration_new ((GtkWindow *)shell);
	}


	priv->box = (GtkWidget *) gtk_vbox_new (FALSE, 0);
	gtk_widget_show ((GtkWidget *)priv->box);
	
	/* Toolbar */
	priv->top_bar = gtk_toolbar_new ();
	gtk_box_pack_start ((GtkBox *)priv->box, priv->top_bar, FALSE, FALSE, 0);
	gtk_widget_show (priv->top_bar);
	if (g_getenv("ANJAL_NO_MAX") || windowed) {
		gtk_container_set_border_width (GTK_CONTAINER (shell), 1);
		g_signal_connect (priv->top_bar, "expose-event",
						  G_CALLBACK (color_expose),
						  shell);
		/* Leave it to the theme to decide the height */
		/* gtk_widget_set_size_request (priv->top_bar, -1, 42);	*/
	}

	/* Check mail button and the spinner */
	priv->check_mail = (GtkWidget *)gtk_tool_button_new (NULL, NULL);
	gtk_widget_set_tooltip_text (priv->check_mail, _("Check Mail"));
	box = gtk_hbox_new (FALSE, 0);
	tmp = e_spinner_new_spinning_small_shown();
	gtk_widget_hide(tmp);
	g_object_set_data ((GObject *)priv->check_mail, "spinner", tmp);
	gtk_box_pack_start ((GtkBox *)box, tmp, FALSE, FALSE, 0);
	tmp = gtk_image_new_from_icon_name("mail-send-receive", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show(tmp);
	g_object_set_data ((GObject *)priv->check_mail, "icon", tmp);
	gtk_box_pack_start ((GtkBox *)box, tmp, FALSE, FALSE, 0);
	gtk_widget_show(box);
	gtk_tool_button_set_icon_widget ((GtkToolButton *)priv->check_mail, box);
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->check_mail, 0);
	gtk_widget_show (priv->check_mail);
	g_signal_connect (priv->check_mail, "clicked", G_CALLBACK(ms_check_mail), shell);

	/* New mail tool button */
	PACK_IN_TOOL(priv->new_mail, "mail-message-new");
	gtk_widget_set_tooltip_text (priv->new_mail, _("New Mail"));
	gtk_widget_show_all (priv->new_mail);
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->new_mail, 1);

	/* Separator before Search */
	tmp = (GtkWidget *)gtk_separator_tool_item_new ();
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)tmp, 2);
	gtk_separator_tool_item_set_draw  ((GtkSeparatorToolItem *)tmp, FALSE);
	gtk_widget_set_size_request (tmp, 20, -1);
	gtk_widget_show (tmp);

	/* Search bar */
	priv->search_bar = (GtkWidget *)gtk_tool_item_new ();
	gtk_tool_item_set_expand ((GtkToolItem *)priv->search_bar, TRUE);
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->search_bar, 3);
	gtk_widget_show (priv->search_bar);
	tmp = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (tmp);
	priv->search_entry = (GtkWidget *)mail_search_new ();
	((MailSearch *)priv->search_entry)->shell = shell;
	gtk_widget_show (priv->search_entry);
	gtk_box_pack_start ((GtkBox *)tmp, priv->search_entry, TRUE, FALSE, 0);
	gtk_container_add ((GtkContainer *)priv->search_bar, tmp);

	/* Post search separator */
	tmp = (GtkWidget *)gtk_separator_tool_item_new ();
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)tmp, 4);
	gtk_separator_tool_item_set_draw  ((GtkSeparatorToolItem *)tmp, FALSE);
	gtk_widget_set_size_request (tmp, 20, -1);
	gtk_widget_show (tmp);

	/* Sort combo */
	tmp = gtk_hbox_new (FALSE, 0);
	lbl =  gtk_label_new (_("Sort By"));
	ar1 = gtk_arrow_new(GTK_ARROW_DOWN, GTK_SHADOW_NONE);

#if 0	
	if (windowed ||  g_getenv("ANJAL_NO_MAX") != NULL) {
		GdkColor color;

		gdk_color_parse ("#000000", &color);
		gtk_widget_modify_fg (lbl, GTK_STATE_SELECTED, &color);
		gtk_widget_modify_fg (lbl, GTK_STATE_ACTIVE, &color);
		gtk_widget_modify_fg (lbl, GTK_STATE_PRELIGHT, &color);

		gdk_color_parse ("#ffffff", &color);
		gtk_widget_modify_fg (lbl, GTK_STATE_NORMAL, &color);
	}
#endif

	gtk_box_pack_start ((GtkBox *)tmp, lbl, FALSE, FALSE, 6);
	gtk_box_pack_start ((GtkBox *)tmp, ar1, FALSE, FALSE, 0);
	gtk_widget_show_all (tmp);
	priv->sort = (GtkWidget *)gtk_tool_button_new  (tmp, NULL);
	gtk_widget_show_all (priv->sort);
	g_signal_connect (priv->sort, "clicked", G_CALLBACK(popup_sort), shell);
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->sort, 5);

	/* Settings button */
	PACK_IN_TOOL(priv->settings, "preferences-system");
	gtk_widget_set_tooltip_text(priv->settings, _("Settings"));
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->settings, 6);
	gtk_widget_show_all (priv->settings);
	g_signal_connect (priv->settings, "clicked", G_CALLBACK (settings_btn_clicked), shell);

	/* Post Settings separator */
	tmp = (GtkWidget *)gtk_separator_tool_item_new ();
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)tmp, 7);
	gtk_separator_tool_item_set_draw  ((GtkSeparatorToolItem *)tmp, FALSE);
	gtk_widget_show (tmp);
	
	/* Close button */
	PACK_IN_TOOL(priv->quit, "gtk-close");
	gtk_widget_set_tooltip_text(priv->quit, _("Quit"));
	gtk_toolbar_insert ((GtkToolbar *)priv->top_bar, (GtkToolItem *)priv->quit, 8);
	gtk_widget_show_all (priv->quit);
	g_signal_connect (priv->quit, "clicked", G_CALLBACK(mail_shell_quit_cb), shell);
	
	gtk_container_add ((GtkContainer *)shell, priv->box);

	shell->mail_component = mail_component_create ((GtkWidget *)shell, FALSE);
	
	priv->side_pane = gtk_hpaned_new ();
	tmp = gtk_vbox_new (FALSE, 0);
	mail_view_set_folder_tree_widget ((MailView *)shell->mail_component->mail_view, tmp);
	((MailSearch *)priv->search_entry)->view = (MailView *)shell->mail_component->mail_view;	
	mail_view_init_search ((MailView *)shell->mail_component->mail_view, priv->search_entry);
	g_signal_connect ((GObject *)shell->mail_component->mail_view, "view-new", G_CALLBACK(handle_cmdline), shell);

	shell->priv->folder_pane = tmp;
	gtk_paned_add1 ((GtkPaned *)priv->side_pane, tmp);
	gtk_widget_show (priv->side_pane);
	gtk_widget_show (tmp);

#if HAVE_ANERLEY
	img = gtk_button_new_with_label(_("People"));
	g_signal_connect (img, "clicked", G_CALLBACK(ms_show_people), shell);
	gtk_widget_show (img);
	gtk_box_pack_end ((GtkBox *)tmp, img, FALSE, FALSE, 0);
#endif

	gtk_box_pack_start ((GtkBox *)tmp, shell->mail_component->folder_tree, TRUE, TRUE, 6);
	gtk_widget_set_size_request (shell->mail_component->folder_tree, 200, 300);
	mail_view_set_folder_tree ((MailView *)shell->mail_component->mail_view, shell->mail_component->folder_tree);
	gtk_widget_show (shell->mail_component->folder_tree);

	tmp = gtk_button_new ();
	box = gtk_hbox_new (FALSE, 0);
	ar1 = gtk_arrow_new(GTK_ARROW_LEFT, GTK_SHADOW_NONE);
	ar2 = gtk_arrow_new(GTK_ARROW_RIGHT, GTK_SHADOW_NONE);
	gtk_box_pack_start ((GtkBox *)box, ar1, FALSE, FALSE, 0);
	gtk_box_pack_start ((GtkBox *)box, ar2, FALSE, FALSE, 0);
	gtk_container_add ((GtkContainer *)tmp, box);
	gtk_widget_show (box);
	gtk_widget_hide (tmp);
	gtk_widget_hide (ar1);
	gtk_widget_show (ar2);
	box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start ((GtkBox *)box, tmp, FALSE, FALSE, 0);
	priv->slider = tmp;
	g_object_set_data ((GObject *)priv->slider, "left-arrow", ar1);
	g_object_set_data ((GObject *)priv->slider, "right-arrow", ar2);
	gtk_box_pack_start ((GtkBox *)box, shell->mail_component->mail_view, TRUE, TRUE, 0);
	gtk_widget_show (shell->mail_component->mail_view);
	gtk_widget_show (box);
	gtk_paned_add2 ((GtkPaned *)priv->side_pane, box);
	tmp = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (tmp);
	gtk_box_pack_start ((GtkBox *)tmp, priv->side_pane, TRUE, TRUE, 2);
	gtk_box_pack_start ((GtkBox *)priv->box, tmp, TRUE, TRUE, 6);

	g_signal_connect (priv->new_mail, "clicked", G_CALLBACK (new_email_btn_clicked), 
					  shell);
	mail_view_set_slider ((MailView *)shell->mail_component->mail_view, priv->slider);
	mail_view_set_sort_by ((MailView *)shell->mail_component->mail_view, priv->sort);
	mail_view_set_check_email ((MailView *)shell->mail_component->mail_view, priv->check_mail);

	gtk_box_pack_end ((GtkBox *)priv->box, shell->mail_component->status_bar, FALSE, FALSE, 2);
	if (ms_check_new()) {
		MailViewChild *mc;
		char *pdir = g_build_filename (g_get_home_dir(), ".gnome2_private", NULL);

		gtk_widget_hide (((MailView *) shell->mail_component->mail_view)->folder_tree);
		mc = mail_view_add_page ((MailView *)shell->mail_component->mail_view, MAIL_VIEW_ACCOUNT, NULL);
		g_signal_connect (mc, "view-close", G_CALLBACK(ms_show_post_druid), shell);
		setup_abooks ();
		if (!g_file_test(pdir, G_FILE_TEST_EXISTS)) {
			g_mkdir (pdir, 0700);
		}
		g_free (pdir);
	}
}
#endif

int
mail_shell_toolbar_height (MailShell *shell)
{
	return shell->priv->top_bar->allocation.height;
}

MailShell *
mail_shell_new (EShell *eshell, gboolean mode)
{
	MailShell *shell = g_object_new (MAIL_SHELL_TYPE, "shell", eshell, "safe-mode", mode, NULL);

	default_shell = shell;
	//mail_shell_construct (shell);

	return shell;
}

void 
mail_shell_set_cmdline_args (MailShell *shell, char **args)
{
	shell->priv->args = args;
}

void e_msg_composer_set_lite (void);

#define PERSONAL_RELATIVE_URI "system"

static void
setup_abooks()
{
	char *base_dir, *uri;
	GSList *groups;
	ESourceGroup *group;
	ESourceList *list = NULL;
	ESourceGroup *on_this_computer = NULL;
	ESource *personal_source = NULL;

	base_dir = g_build_filename (e_get_user_data_dir (), "addressbook", "local", NULL);
	uri = g_filename_to_uri (base_dir, NULL, NULL);
	
	if (!e_book_get_addressbooks(&list, NULL)) {
		g_warning ("Unable to get books\n");
		return;
	}

	groups = e_source_list_peek_groups (list);
	if (groups) {
		/* groups are already there, we need to search for things... */
		GSList *g;

		for (g = groups; g; g = g->next) {

			group = E_SOURCE_GROUP (g->data);

			if (!on_this_computer && !strcmp (uri, e_source_group_peek_base_uri (group))) {
				on_this_computer = g_object_ref (group);
				break;
			}
		}
	}
	
	if (on_this_computer) {
		/* make sure "Personal" shows up as a source under
		   this group */
		GSList *sources = e_source_group_peek_sources (on_this_computer);
		GSList *s;
		for (s = sources; s; s = s->next) {
			ESource *source = E_SOURCE (s->data);
			const gchar *relative_uri;

			relative_uri = e_source_peek_relative_uri (source);
			if (relative_uri == NULL)
				continue;
			if (!strcmp (PERSONAL_RELATIVE_URI, relative_uri)) {
				personal_source = g_object_ref (source);
				break;
			}
		}
	}
	else {
		/* create the local source group */
		group = e_source_group_new (_("On This Computer"), uri);
		e_source_list_add_group (list, group, -1);

		on_this_computer = group;
	}

	if (!personal_source) {
		/* Create the default Person addressbook */
		ESource *source = e_source_new (_("Personal"), PERSONAL_RELATIVE_URI);
		e_source_group_add_source (on_this_computer, source, -1);

		e_source_set_property (source, "completion", "true");

		personal_source = source;
	}

	if (on_this_computer)
		g_object_unref (on_this_computer);
	if (personal_source)
		g_object_unref (personal_source);
	
	e_source_list_sync (list, NULL);
	g_object_unref (list);
	g_free (uri);
	g_free (base_dir);
}

void
mail_shell_handle_cmdline (MailShell *shell)
{
	int i;
	
	if (!shell->priv->args)
		return;

	e_msg_composer_set_lite ();
	for (i=0; shell->priv->args[i]; i++) {
		if (strncmp (shell->priv->args[i], "mailto:", 7) == 0) {
			/* Handle mailto:// */
			if (em_utils_check_user_can_send_mail()) {
				GtkWidget *composer = (GtkWidget *) em_utils_compose_new_message_with_mailto ((const char *)shell->priv->args[i], NULL);
				MailViewChild *child = mail_view_add_page ((MailView *)shell->priv->view, MAIL_VIEW_COMPOSER, (gpointer)composer);
				child->flags |= MAIL_VIEW_HOLD_FOCUS;
			} else {
				g_message ("Please configure an account before anything else");
			}
		}
	}
	/* FIXME: Check who own the memory? GOptionContext or me ? */
	shell->priv->args = NULL;
}
