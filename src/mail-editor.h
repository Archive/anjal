/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Johnny Jacob <jjohnny@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef __MAIL_EDITOR_H
#define __MAIL_EDITOR_H

#include <gtk/gtk.h>
#include <webkit/webkit.h>

#define MAIL_EDITOR_TYPE \
	(mail_editor_get_type ())
#define MAIL_EDITOR(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), MAIL_EDITOR_TYPE, MailEditor))
#define MAIL_EDITOR_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), MAIL_EDITOR_TYPE, MailEditorClass))
#define IS_MAIL_EDITOR(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), MAIL_EDITOR_TYPE))
#define IS_MAIL_EDITOR_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((obj), MAIL_EDITOR_TYPE))
#define MAIL_EDITOR_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), MAIL_EDITOR_TYPE, MailEditorClass))

typedef struct _MailEditor MailEditor;
typedef struct _MailEditorClass MailEditorClass;
typedef struct _MailEditorPrivate MailEditorPrivate;

struct _MailEditor {
        GtkVBox parent; /*Till Webkits*/

	MailEditorPrivate *priv;
};

struct _MailEditorClass {
	GtkVBoxClass parent_class;
};

GtkWidget *mail_editor_new(void);

gchar *mail_editor_get_text_plain (MailEditor *editor, gsize *length);
gchar* mail_editor_get_html (MailEditor *editor, gsize *length);
WebKitWebView * mail_editor_get_webview (MailEditor *editor); 
#endif 
