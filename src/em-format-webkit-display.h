/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 */

/*
  Concrete class for formatting mails to displayed html
*/

#ifndef _EM_FORMAT_WEBKIT_DISPLAY_H
#define _EM_FORMAT_WEBKIT_DISPLAY_H

#include "em-format-webkit.h"
#include <misc/e-attachment.h>

typedef struct _EMFormatWebKitDisplay EMFormatWebKitDisplay;
typedef struct _EMFormatWebKitDisplayClass EMFormatWebKitDisplayClass;

struct _CamelMimePart;

struct _EMFormatWebKitDisplay {
	EMFormatWebKit formathtml;

	struct _EMFormatWebKitDisplayPrivate *priv;

	struct _ESearchingTokenizer *search_tok;

	unsigned int animate:1;
	unsigned int caret_mode:1;
	unsigned int nobar:1;
};

#define EM_FORMAT_WEBKIT_DISPLAY_SEARCH_PRIMARY (0)
#define EM_FORMAT_WEBKIT_DISPLAY_SEARCH_SECONDARY (1)
#define EM_FORMAT_WEBKIT_DISPLAY_SEARCH_ICASE (1<<8)

struct _EMFormatWebKitDisplayClass {
	EMFormatWebKitClass formathtml_class;

	/* a link clicked normally */
	void (*link_clicked)(EMFormatWebKitDisplay *efwd, const char *uri);
	/* a part or a link button pressed event */
	int (*popup_event)(EMFormatWebKitDisplay *efwd, GdkEventButton *event, const char *uri, struct _CamelMimePart *part);
	/* the mouse is over a link */
	void (*on_url)(EMFormatWebKitDisplay *efwd, const char *uri);
};

GType em_format_webkit_display_get_type(void);
EMFormatWebKitDisplay *em_format_webkit_display_new(void);

void em_format_webkit_display_goto_anchor(EMFormatWebKitDisplay *efwd, const char *name);

void em_format_webkit_display_set_animate(EMFormatWebKitDisplay *efwd, gboolean state);
void em_format_webkit_display_set_caret_mode(EMFormatWebKitDisplay *efwd, gboolean state);

void em_format_webkit_display_set_search(EMFormatWebKitDisplay *efwd, int type, GSList *strings);
void em_format_webkit_display_search(EMFormatWebKitDisplay *efwd);
void em_format_webkit_display_search_with (EMFormatWebKitDisplay *efwd, char *word);
void em_format_webkit_display_search_close (EMFormatWebKitDisplay *efwd);

GtkWidget *em_format_webkit_get_search_dialog (EMFormatWebKitDisplay *efwd);

void em_format_webkit_display_cut (EMFormatWebKitDisplay *efwd);
void em_format_webkit_display_copy (EMFormatWebKitDisplay *efwd);
void em_format_webkit_display_paste (EMFormatWebKitDisplay *efwd);

void em_format_webkit_display_zoom_in (EMFormatWebKitDisplay *efwd);
void em_format_webkit_display_zoom_out (EMFormatWebKitDisplay *efwd);
void em_format_webkit_display_zoom_reset (EMFormatWebKitDisplay *efwd);

gboolean em_format_webkit_display_popup_menu (EMFormatWebKitDisplay *efwd);

/* experimental */
struct _EPopupExtension;
void em_format_webkit_display_set_popup(EMFormatWebKitDisplay *, struct _EPopupExtension *);

#endif /* !_EM_FORMAT_WEBKIT_DISPLAY_H */
