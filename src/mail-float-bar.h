/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa  Ragavan <sragavan@gnome.org>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef __MAIL_FLOAT_BAR_H
#define __MAIL_FLOAT_BAR_H

#include <glib.h>
#include <gtk/gtk.h>

#define MAIL_FLOAT_BAR_TYPE \
	(mail_float_bar_get_type ())
#define MAIL_EDITOR(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), MAIL_FLOAT_BAR_TYPE, MailFloatBar))
#define MAIL_FLOAT_BAR_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), MAIL_FLOAT_BAR_TYPE, MailFloatBarClass))
#define IS_MAIL_EDITOR(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), MAIL_FLOAT_BAR_TYPE))
#define IS_MAIL_FLOAT_BAR_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((obj), MAIL_FLOAT_BAR_TYPE))
#define MAIL_FLOAT_BAR_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), MAIL_FLOAT_BAR_TYPE, MailFloatBarClass))

typedef struct _MailFloatBar MailFloatBar;
typedef struct _MailFloatBarClass MailFloatBarClass;
typedef struct _MailFloatBarPrivate MailFloatBarPrivate;

struct _MailFloatBar {
        GObject parent; 

	MailFloatBarPrivate *priv;
};

struct _MailFloatBarClass {
	GObjectClass parent_class;
};

MailFloatBar *mail_float_bar_new(GtkWidget *top);

#endif 
