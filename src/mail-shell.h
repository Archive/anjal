/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com>
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifndef _MAIL_SHELL_H_
#define _MAIL_SHELL_H_

#include <shell/e-shell-window.h>
#include "mail-view.h"

#define MAIL_SHELL_TYPE        (mail_shell_get_type ())
#define MAIL_SHELL(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), MAIL_SHELL_TYPE, MailShell))
#define MAIL_SHELL_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), MAIL_SHELL_TYPE, MailShellClass))
#define IS_MAIL_SHELL(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAIL_SHELL_TYPE))
#define IS_MAIL_SHELL_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), MAIL_SHELL_TYPE))
#define MAIL_SHELL_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), MAIL_SHELL_TYPE, MailShellClass))

typedef struct _MailShellPrivate MailShellPrivate;

typedef struct _MailShell {
	EShellWindow parent;
	/* GtkWindow parent; */
	//MailComponent *mail_component;
	
	MailShellPrivate *priv;
} MailShell;

typedef struct _MailShellClass {
	EShellWindowClass parent_class;

	void (* backspace_pressed)    (MailShell *class);
	void (* ctrl_w_pressed)    (MailShell *class);
	void (* slash_pressed)    (MailShell *class);	
	void (* ctrl_q_pressed)    (MailShell *class);	
	void (* f5_pressed)    (MailShell *class);	

} MailShellClass;

MailShell * mail_shell_new (EShell *shell, gboolean safe);
MailViewChild *mail_shell_create_composer (void);
void mail_shell_set_cmdline_args (MailShell *shell, char **args);
void mail_shell_handle_cmdline (MailShell *shell);
MailViewChild * mail_shell_create_composer_mailto (const char *uri);
int mail_shell_toolbar_height (MailShell *shell);

#endif


