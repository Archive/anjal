/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>  
 *
 *
 * Authors:
 *		Srinivasa Ragavan <sragavan@novell.com
 *
 * Copyright (C) 2009 Novell, Inc. (www.novell.com)
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <gtk/gtk.h>
#include "em-webkit-stream.h"

#define d(x)

static void em_webkit_stream_class_init (EMWebKitStreamClass *klass);
static void em_webkit_stream_init (CamelObject *object);
static void em_webkit_stream_finalize (CamelObject *object);

static ssize_t emws_sync_write(CamelStream *stream, const char *buffer, size_t n);
static int emws_sync_close(CamelStream *stream);
static int emws_sync_flush(CamelStream *stream);
static void emws_weak_notify (EMWebKitStream *emws, GObject *view);

static EMSyncStreamClass *parent_class = NULL;

CamelType
em_webkit_stream_get_type (void)
{
	static CamelType type = CAMEL_INVALID_TYPE;

	if (type == CAMEL_INVALID_TYPE) {
		parent_class = (EMSyncStreamClass *)em_sync_stream_get_type();
		type = camel_type_register (em_sync_stream_get_type(),
					    "EMWebKitStream",
					    sizeof (EMWebKitStream),
					    sizeof (EMWebKitStreamClass),
					    (CamelObjectClassInitFunc) em_webkit_stream_class_init,
					    NULL,
					    (CamelObjectInitFunc) em_webkit_stream_init,
					    (CamelObjectFinalizeFunc) em_webkit_stream_finalize);
	}

	return type;
}

static void
em_webkit_stream_class_init (EMWebKitStreamClass *klass)
{
	((EMSyncStreamClass *)klass)->sync_write = emws_sync_write;
	((EMSyncStreamClass *)klass)->sync_flush = emws_sync_flush;
	((EMSyncStreamClass *)klass)->sync_close = emws_sync_close;
}

static void
em_webkit_stream_init (CamelObject *object)
{
	/*EMWebKitStream *emws = (EMWebKitStream *)object;*/
}

#ifdef NOT_USED
static void
emws_cleanup(EMWebKitStream *emws)
{
	emws->sync.cancel = TRUE;	
}
#endif

static void
em_webkit_stream_finalize (CamelObject *object)
{
	EMWebKitStream *emws = (EMWebKitStream *)object;

	if (emws->view) {
		g_object_weak_unref ((GObject *)emws->view, (GWeakNotify)emws_weak_notify, object);
	}
	g_string_free (emws->buffer, TRUE);
	camel_stream_close((CamelStream *)emws);
}

static ssize_t
emws_sync_write(CamelStream *stream, const char *buffer, size_t n)
{
	EMWebKitStream *emws = EM_WEBKIT_STREAM (stream);

	g_string_append (emws->buffer, buffer);
	if (emws->view)
		webkit_web_view_load_html_string (emws->view, emws->buffer->str, "");
	return (ssize_t) n;
}

static int
emws_sync_flush(CamelStream *stream)
{
	/* EMWebKitStream *emws = (EMWebKitStream *)stream;*/

	return 0;
}

static int
emws_sync_close(CamelStream *stream)
{
	EMWebKitStream *emws = (EMWebKitStream *)stream;

#if HAVE_WEBKIT_PATCHED	

	if (emws->view && WEBKIT_IS_WEB_VIEW (emws->view)&&webkit_web_frame_get_height(webkit_web_view_get_main_frame(emws->view)))
		gtk_widget_set_size_request ((GtkWidget *)emws->view, -1, webkit_web_frame_get_height(webkit_web_view_get_main_frame(emws->view)));
#else
	if (emws->view && WEBKIT_IS_WEB_VIEW (emws->view)) {
		GtkRequisition req;

		gtk_widget_size_request ((GtkWidget *)emws->view, &req);

		gtk_widget_set_size_request ((GtkWidget *)emws->view, -1, req.height+10);
	}

#endif	
	return 0;
}

static void
emws_weak_notify (EMWebKitStream *emws, GObject *view)
{
	emws->view = NULL;
}

CamelStream *
em_webkit_stream_new(WebKitWebView *view, GtkWidget *body)
{
	EMWebKitStream *new;

	new = EM_WEBKIT_STREAM (camel_object_new (EM_WEBKIT_STREAM_TYPE));
	new->flags = 0;
	new->view = view;
	new->buffer = g_string_new (NULL);
	new->body = body;
	em_sync_stream_set_buffer_size(&new->sync, 8192);

	g_object_weak_ref ((GObject *)view, (GWeakNotify)emws_weak_notify, new);

	return (CamelStream *)new;
}

void
em_webkit_stream_set_flags (EMWebKitStream *emws, int flags)
{
	emws->flags = flags;
}

GtkWidget *
em_webkit_stream_get_body (EMWebKitStream *emws)
{
	return emws->body;
}
